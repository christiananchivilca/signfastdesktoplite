﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace SignFastCore
{
    public class DefaultUsoNoRepudioVerificador : CertificadoVerificador
    {
        private SFCCertificado privateKey;

        private EstadoCertificadoVerificador estadoVerificacion;


        public DefaultUsoNoRepudioVerificador()
        {

        }

        public DefaultUsoNoRepudioVerificador(SFCCertificado privateKey)
        {
            this.privateKey = privateKey;
        }

        public EstadoCertificadoVerificador obtenerEstadoValidacion()
        {
            return this.estadoVerificacion;
        }

        public EstadoCertificadoVerificador validar()
        {
            if (privateKey == null)
            {
                throw new System.ArgumentNullException("El atributo privateKey se encuentra nulo.");
            }

            

            List<X509KeyUsageExtension> extensions = privateKey.x509Certificate.Extensions.OfType<X509KeyUsageExtension>().ToList();
            foreach (X509Extension extension in extensions)
            {
                X509KeyUsageExtension x509KeyUsageExtension = (X509KeyUsageExtension)extension;
                if ((x509KeyUsageExtension.KeyUsages & X509KeyUsageFlags.EncipherOnly) != X509KeyUsageFlags.None ||
                    (x509KeyUsageExtension.KeyUsages & X509KeyUsageFlags.KeyEncipherment) != X509KeyUsageFlags.None &&
                    (x509KeyUsageExtension.KeyUsages & X509KeyUsageFlags.NonRepudiation) == X509KeyUsageFlags.None)
                {
                    return actualizarEstadoVerificacion(EstadoCertificadoVerificador.SOLO_CIFRADO);
                }

                if ((x509KeyUsageExtension.KeyUsages & X509KeyUsageFlags.NonRepudiation) == X509KeyUsageFlags.None)
                {
                    return actualizarEstadoVerificacion(EstadoCertificadoVerificador.NO_ES_DE_USO_NO_REPUDIO);
                }

                /*To do comentado porque no lista DNI E*/
                //if ((x509KeyUsageExtension.KeyUsages & X509KeyUsageFlags.DigitalSignature) == X509KeyUsageFlags.None)
                //{
                //    return actualizarEstadoVerificacion(EstadoCertificadoVerificador.NO_ES_DE_USO_NO_REPUDIO);
                //}
                /*To do comentado porque no lista DNI E*/

                //if ( ((x509KeyUsageExtension.KeyUsages & X509KeyUsageFlags.NonRepudiation) != X509KeyUsageFlags.None) &&
                //    (x509KeyUsageExtension.KeyUsages & X509KeyUsageFlags.EncipherOnly) != X509KeyUsageFlags.None) 
                //{
                //    return actualizarEstadoVerificacion(EstadoCertificadoVerificador.NO_ES_DE_USO_NO_REPUDIO);
                //}

                //if ((x509KeyUsageExtension.KeyUsages & X509KeyUsageFlags.EncipherOnly) != X509KeyUsageFlags.None)
                //{
                //    return actualizarEstadoVerificacion(EstadoCertificadoVerificador.SOLO_CIFRADO);
                //}
            }

            return actualizarEstadoVerificacion(EstadoCertificadoVerificador.VALIDO);
        }

        private EstadoCertificadoVerificador actualizarEstadoVerificacion(EstadoCertificadoVerificador estadoVerificacion)
        {
            this.estadoVerificacion = estadoVerificacion;
            return this.estadoVerificacion;
        }
    }
}
