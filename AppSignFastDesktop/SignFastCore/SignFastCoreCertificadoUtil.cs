﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography.X509Certificates;
using System.Net;
using System.Security.Cryptography;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;
using System.ComponentModel;
using Org.BouncyCastle.X509;
using Org.BouncyCastle.Crypto;

namespace SignFastCore
{
    class SignFastCoreCertificadoUtil
    {
        private const string CERT_CRL_EXTENSION = "2.5.29.31";

        private const string CRL_CRL_EXTENSION = "2.5.29.46";

        private static WebClient webClient = new WebClient();

        public static List<SFCCertificado> listarCertificadosBibliotecaWindows()
        {
            var listaCertificados = new List<SFCCertificado>();

            SFCCertificado sfcCertificado = null;

            X509Store store = new X509Store(StoreName.My, StoreLocation.CurrentUser);
            store.Open(OpenFlags.ReadOnly | OpenFlags.OpenExistingOnly);

            foreach (X509Certificate2 certificado in store.Certificates)
            {
                if (certificado.HasPrivateKey)
                {
                    sfcCertificado = new SFCCertificado();
                    sfcCertificado.x509Certificate = certificado;
                    sfcCertificado.alias = obtenerAliasCertificado(certificado);

                    listaCertificados.Add(sfcCertificado);
                }
            }

            store.Close();

            return listaCertificados;

        }

        public static SFCCertificado obtenerCertificadoDeFichero(String rutaFicheroCertificado, String password)
        {
            X509Certificate2 certificado = new X509Certificate2(@rutaFicheroCertificado, password);

            SFCCertificado sfcCertificado = new SFCCertificado();
            sfcCertificado.x509Certificate = certificado;
            sfcCertificado.alias = obtenerAliasCertificado(certificado);

            return sfcCertificado;
        }

        public static String obtenerAliasCertificado(X509Certificate2 certificado)
        {
            if (!certificado.HasPrivateKey)
            {
                throw new System.ArgumentNullException("El atributo certificado está nulo");
            }

            if (String.IsNullOrEmpty(certificado.FriendlyName))
            {
                return certificado.GetNameInfo(X509NameType.SimpleName, false);
            }

            return certificado.FriendlyName;
        }

        /** métodos validación CRL */
        public static bool estaCrlExpirado(X509Certificate2 certificado)
        {
            X509CrlParser x509CrlParser = new X509CrlParser();
            X509Crl x509Crl = x509CrlParser.ReadCrl(obtenerCertificadoCrl(certificado));

            DateTime fechaActual = DateTime.Now;
            var fechaExpiracion = x509Crl.NextUpdate;

            int resultado = DateTime.Compare(fechaActual, fechaExpiracion.Value);

            if (resultado < 0)
            {
                return false;
            }

            return true;
        }

        public static bool estaCrlExpirado(X509Certificate2 certificado, String urlCrlCertificado)
        {
            X509CrlParser x509CrlParser = new X509CrlParser();
            X509Crl x509Crl = x509CrlParser.ReadCrl(obtenerCertificadoCrl(certificado, urlCrlCertificado));

            DateTime fechaActual = DateTime.Now;
            var fechaExpiracion = x509Crl.NextUpdate;

            int resultado = DateTime.Compare(fechaActual, fechaExpiracion.Value);

            if (resultado < 0)
            {
                return false;
            }

            return true;
        }

        public static void crlAdulterada(X509Certificate2 certificado, String urlCrlCertificado)
        {

            X509CertificateParser certParser = new X509CertificateParser();
            Org.BouncyCastle.X509.X509Certificate privateCertBouncy = certParser.ReadCertificate(certificado.GetRawCertData());
            AsymmetricKeyParameter publicKey = privateCertBouncy.GetPublicKey();

            X509CrlParser x509CrlParser = new X509CrlParser();
            X509Crl x509Crl = x509CrlParser.ReadCrl(obtenerCertificadoCrl(certificado, urlCrlCertificado));

            x509Crl.Verify(publicKey);

        }

        public static bool estaRevocadoPorCrl(X509Certificate2 cert)
        {
            try
            {
                string certCrlUrl = obtenerUrlCrlCertificado(cert);
                return estaRevocadoPorCrl(cert, certCrlUrl);
            }
            catch
            {
                return false;
            }
        }

        public static bool estaRevocadoPorCrl(X509Certificate2 cert, String url)
        {
            WebClient wc = new WebClient();
            byte[] rgRawCrl = wc.DownloadData(url);
            wc.Dispose();

            IntPtr phCertStore = IntPtr.Zero;
            IntPtr pvContext = IntPtr.Zero;
            GCHandle hCrlData = new GCHandle();
            GCHandle hCryptBlob = new GCHandle();
            try
            {
                hCrlData = GCHandle.Alloc(rgRawCrl, GCHandleType.Pinned);
                WinCrypt32.CRYPTOAPI_BLOB stCryptBlob;
                stCryptBlob.cbData = rgRawCrl.Length;
                stCryptBlob.pbData = hCrlData.AddrOfPinnedObject();
                hCryptBlob = GCHandle.Alloc(stCryptBlob, GCHandleType.Pinned);

                if (!WinCrypt32.CryptQueryObject(
                WinCrypt32.CERT_QUERY_OBJECT_BLOB,
                hCryptBlob.AddrOfPinnedObject(),
                WinCrypt32.CERT_QUERY_CONTENT_FLAG_CRL,
                WinCrypt32.CERT_QUERY_FORMAT_FLAG_BINARY,
                0,
                IntPtr.Zero,
                IntPtr.Zero,
                IntPtr.Zero,
                ref phCertStore,
                IntPtr.Zero,
                ref pvContext
                ))
                {
                    throw new Win32Exception(Marshal.GetLastWin32Error());
                }

                WinCrypt32.CRL_CONTEXT stCrlContext = (WinCrypt32.CRL_CONTEXT)Marshal.PtrToStructure(pvContext, typeof(WinCrypt32.CRL_CONTEXT));
                WinCrypt32.CRL_INFO stCrlInfo = (WinCrypt32.CRL_INFO)Marshal.PtrToStructure(stCrlContext.pCrlInfo, typeof(WinCrypt32.CRL_INFO));

                if (IsCertificateInCrl(cert, stCrlInfo))
                {
                    return true;
                }
                else
                {
                    url = GetDeltaCrlUrl(stCrlInfo);
                    if (!string.IsNullOrEmpty(url))
                    {
                        return estaRevocadoPorCrl(cert, url);
                    }
                }
            }
            finally
            {
                if (hCrlData.IsAllocated) hCrlData.Free();
                if (hCryptBlob.IsAllocated) hCryptBlob.Free();
                if (!pvContext.Equals(IntPtr.Zero))
                {
                    WinCrypt32.CertFreeCRLContext(pvContext);
                }
            }

            return false;
        }

        private static bool IsCertificateInCrl(X509Certificate2 cert, WinCrypt32.CRL_INFO stCrlInfo)
        {
            IntPtr rgCrlEntry = stCrlInfo.rgCRLEntry;

            for (int i = 0; i < stCrlInfo.cCRLEntry; i++)
            {
                string serial = string.Empty;
                WinCrypt32.CRL_ENTRY stCrlEntry = (WinCrypt32.CRL_ENTRY)Marshal.PtrToStructure(rgCrlEntry, typeof(WinCrypt32.CRL_ENTRY));

                IntPtr pByte = stCrlEntry.SerialNumber.pbData;
                for (int j = 0; j < stCrlEntry.SerialNumber.cbData; j++)
                {
                    Byte bByte = Marshal.ReadByte(pByte);
                    serial = bByte.ToString("X").PadLeft(2, '0') + serial;
                    pByte = (IntPtr)((Int32)pByte + Marshal.SizeOf(typeof(Byte)));
                }
                if (cert.SerialNumber == serial)
                {
                    return true;
                }
                rgCrlEntry = (IntPtr)((Int32)rgCrlEntry + Marshal.SizeOf(typeof(WinCrypt32.CRL_ENTRY)));
            }
            return false;
        }

        public static byte[] obtenerCertificadoCrl(X509Certificate2 x509Certificate2)
        {
            String urlCrlCertificado = obtenerUrlCrlCertificado(x509Certificate2);

            WebClient wc = new WebClient();
            byte[] bytesCertificadoCrl = wc.DownloadData(urlCrlCertificado);
            wc.Dispose();

            return bytesCertificadoCrl;

        }

        public static byte[] obtenerCertificadoCrl(X509Certificate2 x509Certificate2, String urlCrlCertificado)
        {

            using (WebClient client = new WebClient())
            {
                byte[] bytesCertificadoCrl = webClient.DownloadData(urlCrlCertificado);
                return bytesCertificadoCrl;
            }

            //byte[] bytesCertificadoCrl = webClient.DownloadData(urlCrlCertificado);

            //return null;

        }

        public static String obtenerUrlCrlCertificado(X509Certificate2 x509Certificate2)
        {
            try
            {

                return (from X509Extension extension in x509Certificate2.Extensions
                        where extension.Oid.Value.Equals(CERT_CRL_EXTENSION)
                        select GetCrlUrlFromExtension(extension)).Single();
            }
            catch
            {
                return null;
            }
        }

        private static string GetDeltaCrlUrl(WinCrypt32.CRL_INFO stCrlInfo)
        {
            IntPtr rgExtension = stCrlInfo.rgExtension;
            X509Extension deltaCrlExtension = null;

            for (int i = 0; i < stCrlInfo.cExtension; i++)
            {
                WinCrypt32.CERT_EXTENSION stCrlExt = (WinCrypt32.CERT_EXTENSION)Marshal.PtrToStructure(rgExtension, typeof(WinCrypt32.CERT_EXTENSION));

                if (stCrlExt.Value.pbData != IntPtr.Zero && stCrlExt.pszObjId == CRL_CRL_EXTENSION)
                {
                    byte[] rawData = new byte[stCrlExt.Value.cbData];
                    Marshal.Copy(stCrlExt.Value.pbData, rawData, 0, rawData.Length);
                    deltaCrlExtension = new X509Extension(stCrlExt.pszObjId, rawData, stCrlExt.fCritical);
                    break;
                }

                rgExtension = (IntPtr)((Int32)rgExtension + Marshal.SizeOf(typeof(WinCrypt32.CERT_EXTENSION)));
            }
            if (deltaCrlExtension == null)
            {
                return null;
            }
            return GetCrlUrlFromExtension(deltaCrlExtension);
        }

        private static string GetCrlUrlFromExtension(X509Extension extension)
        {
            try
            {
                String urlCrl = null;
                //Regex rx = new Regex("(http://.*crl)|(https://.*crl)");
                Regex rx = new Regex("https://*.crl");
                string raw = new AsnEncodedData(extension.Oid, extension.RawData).Format(false);

                //TODO: revisar este metodo de obtener la CRL.
                Char delimitador = ',';
                string[] cadenas = raw.Split(delimitador);
                foreach (string cadena in cadenas)
                {


                    var match = Regex.Match(cadena, "URL=(.*)");
                    try
                    {
                        urlCrl = match.Groups[1].Value;
                    }
                    catch(Exception e)
                    {
                        urlCrl = "";
                    }
                    
                    

                    //if (!String.IsNullOrEmpty(rx.Match(cadena).Value))
                    //{
                    //    urlCrl = rx.Match(cadena).Value;
                    //}

                }

                return urlCrl;
            }
            catch
            {
                return null;
            }
        }

        public static SFCCertificado loadCertificate(byte[] encoded)
        {
            SFCCertificado certificado = new SFCCertificado();
            certificado.encoded = encoded;

            return certificado;
        }

        public static bool estaCertificadoExpirado(X509Certificate2 certificado)
        {
            DateTime fechaActual = DateTime.Now;
            DateTime fechaExpiracion = certificado.NotAfter;
            //Date fechaExpiracion = certificado.getNotAfter();

            int resultado = DateTime.Compare(fechaActual, fechaExpiracion);
            if (resultado < 0)
            {
                return false;
            }

            return true;
        }

        public static Boolean urlEstaDisponible(String url)
        {
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                request.Method = WebRequestMethods.Http.Head;

                HttpWebResponse response = (HttpWebResponse)request.GetResponse();

                Boolean estaActivo = (response.StatusCode == HttpStatusCode.OK);

                //return (response.StatusCode == HttpStatusCode.OK);
                response.Close();

                return estaActivo;
            }
            catch (System.Exception excepcion)
            {
                return false;
            }

        }

    }
}

