﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SignFastCore
{
    public interface CertificadoVerificador
    {
        EstadoCertificadoVerificador validar();

        EstadoCertificadoVerificador obtenerEstadoValidacion();
    }
}
