﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography.X509Certificates;


namespace SignFastCore
{
    public class DefaultExpiracionVerificador : CertificadoVerificador
    {
        private SFCCertificado privateKey;

        private EstadoCertificadoVerificador estadoVerificacion;

        public DefaultExpiracionVerificador()
        {

        }

        public DefaultExpiracionVerificador(SFCCertificado privateKey)
        {
            this.privateKey = privateKey;
        }

        public EstadoCertificadoVerificador obtenerEstadoValidacion()
        {
            return this.estadoVerificacion;
        }

        public EstadoCertificadoVerificador validar()
        {

            if (this.privateKey == null)
            {
                throw new System.ArgumentNullException("El atributo privateKey se encuentra nulo.");
            }

            X509Chain x509Chain = new X509Chain();
            x509Chain.Build(privateKey.x509Certificate);

            int ultimaPosicion = x509Chain.ChainElements.Count - 1;
            for (int posicion = 0; posicion < x509Chain.ChainElements.Count; posicion++)
            {

                if (SignFastCoreCertificadoUtil.estaCertificadoExpirado(x509Chain.ChainElements[posicion].Certificate))
                {
                    if (posicion == 0)
                    {
                        return actualizarEstadoVerificacion(EstadoCertificadoVerificador.EXPIRADO);
                    }
                    else if (posicion == ultimaPosicion)
                    {
                        return actualizarEstadoVerificacion(EstadoCertificadoVerificador.CA_RAIZ_EXPIRADO);
                    }
                    else
                    {
                        return actualizarEstadoVerificacion(EstadoCertificadoVerificador.CA_INTERMEDIO_EXPIRADO);
                    }
                }
            }
            return actualizarEstadoVerificacion(EstadoCertificadoVerificador.VALIDO);
        }

        private EstadoCertificadoVerificador actualizarEstadoVerificacion(EstadoCertificadoVerificador estadoVerificacion)
        {
            this.estadoVerificacion = estadoVerificacion;
            return this.estadoVerificacion;
        }
    }
}
