﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SignFastCore
{
    public class Indecopi2017CertificadoVerificador
    {
        public SFCCertificado privateKey { get; set; }

        public EstadoCertificadoVerificador respuestaVerificacion { get; set; }

        public Boolean validarTslDesdeArchivo { get; set; }

        public String rutaArchivoTsl { get; set; }

        public String urlTsl { get; set; }

        public Boolean validarTsl { get; set; }

        public Boolean validarNoRepudio { get; set; }

        public Boolean validarCrl { get; set; }

        private String urlCrl;

        public Indecopi2017CertificadoVerificador()
        {
            respuestaVerificacion = EstadoCertificadoVerificador.VALIDO;
            validarTslDesdeArchivo = false;
            validarNoRepudio = false;
            validarTsl = true;
            validarCrl = true;
        }

        public Indecopi2017CertificadoVerificador(SFCCertificado privateKey)
        {
            this.privateKey = privateKey;
            respuestaVerificacion = EstadoCertificadoVerificador.VALIDO;
            validarTslDesdeArchivo = false;
            validarNoRepudio = false;
            validarTsl = false;
            validarCrl = true;
        }

        public Boolean esValidoParaFirmar()
        {

            if (this.privateKey == null)
            {
                throw new System.ArgumentNullException("El atributo privateKey se encuentra nulo.");
            }

            List<CertificadoVerificador> verificadoresCertificado = new List<CertificadoVerificador>();

            if (validarCrl)
            {
                verificadoresCertificado.Add(new DefaultCrlVerificador(privateKey));
            }

            verificadoresCertificado.Add(new DefaultExpiracionVerificador(privateKey));


            if (validarNoRepudio)
            {
                verificadoresCertificado.Add(new DefaultUsoNoRepudioVerificador(privateKey));
            }

            if (validarTsl)
            {
                verificadoresCertificado.Add(new DefaultTslVerificador(privateKey, validarTslDesdeArchivo, rutaArchivoTsl, urlTsl));
            }


            foreach (CertificadoVerificador verificador in verificadoresCertificado)
            {
                if (verificador.validar() != EstadoCertificadoVerificador.VALIDO)
                {
                    respuestaVerificacion = verificador.obtenerEstadoValidacion();
                    if (verificador.GetType() == typeof(DefaultCrlVerificador))
                    {
                        urlCrl = ((DefaultCrlVerificador)verificador).urlCrl;
                    }
                    return false;
                }
            }
            return true;
        }

        public String obtenerMensajeRespuestaVerificacion()
        {
            String respuesta = "Certificado se encuentra válido para firmar.";
            String mensajeGenerico = "El certificado seleccionado no está habilitado para firmar archivos. Motivo: ";

            switch (respuestaVerificacion)
            {
                case EstadoCertificadoVerificador.VALIDO:
                    return "Certificado se encuentra válido para firmar.";
                case EstadoCertificadoVerificador.NO_ES_DE_USO_NO_REPUDIO:
                    return mensajeGenerico + "Certificado no es de propósito no repudio.";
                case EstadoCertificadoVerificador.EXPIRADO:
                    return mensajeGenerico + "Certificado está expirado.";
                case EstadoCertificadoVerificador.CA_RAIZ_EXPIRADO:
                    return mensajeGenerico + "Certificado CA raíz está expirado.";
                case EstadoCertificadoVerificador.CA_INTERMEDIO_EXPIRADO:
                    return mensajeGenerico + "Certificado CA intermedio está expirado.";
                case EstadoCertificadoVerificador.CRL_EXPIRADO:
                    return mensajeGenerico + "CRL del certificado está expirado.";
                case EstadoCertificadoVerificador.CRL_CA_INTERMEDIA_EXPIRADO:
                    return mensajeGenerico + "CRL del certificado CA intermedio está expirado.";
                case EstadoCertificadoVerificador.REVOCADO:
                    return mensajeGenerico + "Certificado está revocado.";
                case EstadoCertificadoVerificador.CA_RAIZ_REVOCADO:
                    return mensajeGenerico + "Certificado CA raíz está revocado.";
                case EstadoCertificadoVerificador.CA_INTERMEDIO_REVOCADO:
                    return mensajeGenerico + "Certificado CA intermedia está revocado.";
                case EstadoCertificadoVerificador.NO_ESTA_EN_TSL:
                    return mensajeGenerico + "Certificado no está en la TSL";
                case EstadoCertificadoVerificador.URL_CRL_NO_DISPONIBLE:
                    return mensajeGenerico + "El Servicio Web no se encuentra disponible (CRL). URL: " + urlCrl;
                case EstadoCertificadoVerificador.NO_ES_DE_USO_FIRMA_DIGITAL:
                    return mensajeGenerico + "El certificado no puede ser usado para firmar documentos.";
                case EstadoCertificadoVerificador.SOLO_CIFRADO:
                    return mensajeGenerico + "El certificado es de sólo de uso para cifrado.";
                case EstadoCertificadoVerificador.CRL_INVALIDA:
                    return mensajeGenerico + "CRL del certificado es invalida.";
                case EstadoCertificadoVerificador.CRL_ADULTERADO:
                    return mensajeGenerico + "CRL del certificado esta adulterado.";
                case EstadoCertificadoVerificador.CRL_CA_INTERMEDIA_ADULTERADO:
                    return mensajeGenerico + "CRL del certificado CA intermedio está adulterado.";
                case EstadoCertificadoVerificador.CRL_ADULTERADO_RAIZ:
                    return mensajeGenerico + "CRL del certificado CA Raíz está adulterado.";
                case EstadoCertificadoVerificador.CRL_ADULTERADO_INTERMEDIO:
                    return mensajeGenerico + "CRL del certificado Intermedio está adulterado.";
                case EstadoCertificadoVerificador.CRL_CA_RAIZ_EXPIRADO:
                    return mensajeGenerico + "CRL del certificado CA Raíz está expirado.";

            }

            return respuesta;
        }
    }
}

