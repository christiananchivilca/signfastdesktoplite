﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using Org.BouncyCastle.Security;
using Org.BouncyCastle.Security.Certificates;
using System.Text;
using System.Threading.Tasks;

namespace SignFastCore
{
    public class DefaultCrlVerificador : CertificadoVerificador
    {
        private SFCCertificado privateKey;

        private EstadoCertificadoVerificador estadoVerificacion;

        public String urlCrl { get; set; }

        public DefaultCrlVerificador()
        {

        }

        public DefaultCrlVerificador(SFCCertificado privateKey)
        {
            this.privateKey = privateKey;
        }

        public EstadoCertificadoVerificador obtenerEstadoValidacion()
        {
            return this.estadoVerificacion;
        }

        public EstadoCertificadoVerificador validar()
        {

            if (this.privateKey == null)
            {
                throw new System.ArgumentNullException("El atributo privateKey se encuentra nulo.");
            }

            X509Chain x509Chain = new X509Chain();
            x509Chain.Build(privateKey.x509Certificate);

            //int ultimaPosicion = x509Chain.ChainElements.Count - 1;
            int ultimaPosicion = x509Chain.ChainElements.Count > 1 ? x509Chain.ChainElements.Count - 1 : x509Chain.ChainElements.Count;
            for (int posicion = 0; posicion < ultimaPosicion; posicion++)
            {

                String urlCrlCertificado = SignFastCoreCertificadoUtil.obtenerUrlCrlCertificado(x509Chain.ChainElements[posicion].Certificate);

                
                if (!SignFastCoreCertificadoUtil.urlEstaDisponible(urlCrlCertificado))
                {
                    urlCrl = urlCrlCertificado;
                    return actualizarEstadoVerificacion(EstadoCertificadoVerificador.URL_CRL_NO_DISPONIBLE);
                }
                

                //if (SignFastCoreCertificadoUtil.estaCrlExpirado(x509Chain.ChainElements[posicion].Certificate, urlCrlCertificado))
                if (SignFastCoreCertificadoUtil.estaCrlExpirado(x509Chain.ChainElements[posicion].Certificate, urlCrlCertificado))
                {
                    if (posicion == 0)
                    {
                        //return actualizarEstadoVerificacion(EstadoCertificadoVerificador.CRL_EXPIRADO);
                        return actualizarEstadoVerificacion(EstadoCertificadoVerificador.CRL_CA_INTERMEDIA_EXPIRADO);
                    }
                    else
                    {
                        //return actualizarEstadoVerificacion(EstadoCertificadoVerificador.CRL_CA_INTERMEDIA_EXPIRADO);
                        return actualizarEstadoVerificacion(EstadoCertificadoVerificador.CRL_CA_RAIZ_EXPIRADO);
                    }
                }

            }

            for (int posicion = 0; posicion < ultimaPosicion; posicion++)
            {

                String urlCrlCertificado = SignFastCoreCertificadoUtil.obtenerUrlCrlCertificado(x509Chain.ChainElements[posicion].Certificate);

                if (x509Chain.ChainElements.Count > 1)
                {
                    //int posicionCrl = ultimaPosicion == 1 ? posicion : posicion + 1;
                    try
                    {
                        SignFastCoreCertificadoUtil.crlAdulterada(x509Chain.ChainElements[posicion + 1].Certificate, urlCrlCertificado);
                    }
                    catch (Exception ex)
                    {
                        if (ex is InvalidKeyException || ex is CrlException)
                        {
                            return actualizarEstadoVerificacion(EstadoCertificadoVerificador.CRL_INVALIDA);
                        }
                        if (ex is SignatureException)
                        {
                            if( posicion== 0) {
                                //return actualizarEstadoVerificacion(EstadoCertificadoVerificador.CRL_ADULTERADO);
                                return actualizarEstadoVerificacion(EstadoCertificadoVerificador.CRL_ADULTERADO_INTERMEDIO);
                            }
                            else
                            {
                                return actualizarEstadoVerificacion(EstadoCertificadoVerificador.CRL_ADULTERADO_RAIZ);
                            }
                        }
                    }
                }

                if (SignFastCoreCertificadoUtil.estaRevocadoPorCrl(x509Chain.ChainElements[posicion].Certificate))
                {
                    if (posicion == 0)
                    {
                        return actualizarEstadoVerificacion(EstadoCertificadoVerificador.REVOCADO);
                    }
                    else
                    {
                        return actualizarEstadoVerificacion(EstadoCertificadoVerificador.CA_INTERMEDIO_REVOCADO);
                    }
                }
            }

            return actualizarEstadoVerificacion(EstadoCertificadoVerificador.VALIDO);
        }

        private EstadoCertificadoVerificador actualizarEstadoVerificacion(EstadoCertificadoVerificador estadoVerificacion)
        {
            this.estadoVerificacion = estadoVerificacion;
            return this.estadoVerificacion;
        }

    }

}

