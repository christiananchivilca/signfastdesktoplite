﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Xml;

namespace SignFastCore
{
    public class IndigitalUtil
    {
        public static bool escribirBytesEnDisco(byte[] bytesArchivo, String rutaArchivo)
        {
            try
            {
                using (var fs = new FileStream(rutaArchivo, FileMode.Create, FileAccess.Write))
                {
                    fs.Write(bytesArchivo, 0, bytesArchivo.Length);
                    return true;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception caught in process: {0}", ex);
                return false;
            }
        }

        public static String obtenerValorElementoXml(String nombreEtiqueta, XmlElement elemento)
        {
            XmlNodeList nodoLista = elemento.GetElementsByTagName(nombreEtiqueta).Item(0).ChildNodes;
            XmlNode primerNodoEncontrado = (XmlNode)nodoLista.Item(0);
            return primerNodoEncontrado.Value;
        }
    }
}
