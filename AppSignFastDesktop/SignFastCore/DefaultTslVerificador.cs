﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Security.Cryptography.X509Certificates;
using System.Net;

namespace SignFastCore
{
    class DefaultTslVerificador : CertificadoVerificador
    {
        private static String DEFAULT_URL_TSL = "https://iofe.indecopi.gob.pe/TSL/tsl-pe.xml";
        private static String DEFAULT_RUTA_TSL = System.IO.Path.GetTempPath() + System.IO.Path.DirectorySeparatorChar + "tsl-pe.xml";
        //private static String DEFAULT_RUTA_TSL = System.IO.Path.GetTempPath() + "tsl-pe.xml";

        private static String TAG_TSL_TSP_SERVICES = "tsl:TSPServices";
        private static String TAG_TSL_X509CERTIFICATE = "tsl:X509Certificate";
        private static String TAG_TSL_TSP_SERVICE = "tsl:TSPService";

        public SFCCertificado privateKey { get; set; }
        public EstadoCertificadoVerificador estadoVerificacion { get; set; }
        public bool validarTslDesdeArchivo { get; set; }
        public String rutaArchivoTsl { get; set; }
        public String urlTsl { get; set; }

        public DefaultTslVerificador()
        {
            this.validarTslDesdeArchivo = false;
        }

        public DefaultTslVerificador(SFCCertificado privateKey)
        {
            this.privateKey = privateKey;
            this.validarTslDesdeArchivo = false;
        }

        public DefaultTslVerificador(SFCCertificado privateKey, bool validarTslDesdeArchivo, String rutaArchivoTsl, String urlTsl)
        {
            this.privateKey = privateKey;
            this.validarTslDesdeArchivo = validarTslDesdeArchivo;
            this.rutaArchivoTsl = rutaArchivoTsl;
            this.urlTsl = urlTsl;
        }

        public EstadoCertificadoVerificador obtenerEstadoValidacion()
        {
            return this.estadoVerificacion;
        }

        public EstadoCertificadoVerificador validar()
        {

            if (this.privateKey == null)
            {
                throw new System.ArgumentNullException("El atributo privateKey se encuentra nulo.");
            }

            X509Chain x509Chain = new X509Chain();
            x509Chain.Build(privateKey.x509Certificate);

            List<SFCCertificado> listaCertificadosTsl = cargarCertificadosDeTsl();
            foreach (SFCCertificado certificateTokenTsl in listaCertificadosTsl)
            {
                String encodedCertificateTsl = Convert.ToBase64String(certificateTokenTsl.encoded);
                //TODO revisar la posicion del CA del certificado, para optimizar la validación
                for (int posicion = 0; posicion < x509Chain.ChainElements.Count; posicion++)
                {
                    String encodedCertificateChain = Convert.ToBase64String(x509Chain.ChainElements[posicion].Certificate.RawData);
                    //Debug.WriteLine("encoded: " + encodedCertificateChain);
                    if (String.Equals(encodedCertificateTsl, encodedCertificateChain))
                    {
                        return actualizarEstadoVerificacion(EstadoCertificadoVerificador.VALIDO);
                    }
                }
            }

            return actualizarEstadoVerificacion(EstadoCertificadoVerificador.NO_ESTA_EN_TSL);
        }

        private List<SFCCertificado> cargarCertificadosDeTsl()
        {

            if (!validarTslDesdeArchivo)
            {
                descargarArchivoTsl();
            }

            List<SFCCertificado> listaCertificadosTsl = new List<SFCCertificado>();

            XmlDocument xmlDocument = new XmlDocument();
            //xmlDocument.LoadXml(rutaArchivoTsl);
            xmlDocument.Load(rutaArchivoTsl);

            //XmlNodeList nodoTSPServices = xmlDocument.GetElementsByTagName(TAG_TSL_TSP_SERVICES);
            XmlNodeList nodoTSPServices = xmlDocument.GetElementsByTagName(TAG_TSL_TSP_SERVICE);

            byte[] bytesEncodedCertificado = null;
            foreach (XmlNode nodoServiceInformation in nodoTSPServices)
            {
                if (nodoServiceInformation.NodeType == XmlNodeType.Element)
                {
                    XmlElement elemento = (XmlElement)nodoServiceInformation;

                    bytesEncodedCertificado = Convert.FromBase64String(IndigitalUtil.obtenerValorElementoXml(TAG_TSL_X509CERTIFICATE, elemento));
                    listaCertificadosTsl.Add(SignFastCoreCertificadoUtil.loadCertificate(bytesEncodedCertificado));
                    //Console.WriteLine(Convert.ToBase64String(bytesEncodedCertificado));
                }
            }

            return listaCertificadosTsl;
        }

        private String descargarArchivoTsl()
        {
            this.rutaArchivoTsl = DEFAULT_RUTA_TSL;
            if (this.urlTsl == null || String.Equals("", this.urlTsl))
            {
                this.urlTsl = DEFAULT_URL_TSL;
            }

            WebClient wc = new WebClient();
            byte[] byteTsl = wc.DownloadData(urlTsl);

            IndigitalUtil.escribirBytesEnDisco(byteTsl, rutaArchivoTsl);

            return rutaArchivoTsl;
        }

        private EstadoCertificadoVerificador actualizarEstadoVerificacion(EstadoCertificadoVerificador estadoVerificacion)
        {
            this.estadoVerificacion = estadoVerificacion;
            return this.estadoVerificacion;
        }
    }
}
