﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SignFastCore
{
    public enum EstadoCertificadoVerificador
    {
        VALIDO,
        NO_ES_DE_USO_NO_REPUDIO,
        EXPIRADO,
        CA_RAIZ_EXPIRADO,
        CA_INTERMEDIO_EXPIRADO,
        CRL_EXPIRADO,
        REVOCADO,
        CA_RAIZ_REVOCADO,
        CA_INTERMEDIO_REVOCADO,
        NO_ESTA_EN_TSL,
        CRL_CA_INTERMEDIA_EXPIRADO,
        CRL_CA_RAIZ_EXPIRADO,
        URL_CRL_NO_DISPONIBLE,
        NO_ES_DE_USO_FIRMA_DIGITAL,
        CRL_INVALIDA,
        CRL_ADULTERADO,
        CRL_ADULTERADO_INTERMEDIO,
        CRL_ADULTERADO_RAIZ,
        CRL_CA_INTERMEDIA_ADULTERADO,
        SOLO_CIFRADO
    }
}
