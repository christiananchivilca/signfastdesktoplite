﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
//using Org.BouncyCastle.X509;
using System.Text;
using System.Threading.Tasks;

namespace SignFastCore
{
    public class SFCCertificado
    {
        public X509Certificate2 x509Certificate { get; set; }

        public String alias { get; set; }

        public byte[] encoded { get; set; }

    }
}
