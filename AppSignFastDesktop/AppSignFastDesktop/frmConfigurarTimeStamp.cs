﻿using AppSignFastDesktop.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AppSignFastDesktop
{
    public partial class frmConfigurarTimeStamp : Form
    {
        public frmConfigurarTimeStamp()
        {
            InitializeComponent();
        }

        private void btnAceptar_Click(object sender, EventArgs e)
        {
            int permisoSelloTiempo = 0;

            if (cbSelloTiempo.Checked)
            {
                permisoSelloTiempo = 1;
            }

            Settings.Default["Usuario"] = txtUsuario.Text;
            Settings.Default["Contrasenia"] = txtContrasenia.Text;
            Settings.Default["Url"] = txtUrl.Text;
            Settings.Default["PermisoSelloTiempo"] = permisoSelloTiempo;
            Settings.Default.Save();

            MessageBox.Show("Los datos fueron guardados exitosamente.", "SignFastDesktop", MessageBoxButtons.OK, MessageBoxIcon.Information);
            this.Close();
        }

        private void frmConfigurarTimeStamp_Load(object sender, EventArgs e)
        {
            if ((int)Settings.Default["PermisoSelloTiempo"] == 1)
            {
                cbSelloTiempo.Checked = true;
                txtUrl.Enabled = true;
                txtUsuario.Enabled = true;
                txtContrasenia.Enabled = true;
            }
            else
            {
                cbSelloTiempo.Checked = false;
                txtUrl.Enabled = false;
                txtUsuario.Enabled = false;
                txtContrasenia.Enabled = false;
            }

                txtUsuario.Text = (string)Settings.Default["Usuario"];
                txtContrasenia.Text = (string)Settings.Default["Contrasenia"];
                txtUrl.Text = (string)Settings.Default["Url"];
        }

        private void cbSelloTiempo_CheckedChanged(object sender, EventArgs e)
        {
            if (cbSelloTiempo.Checked)
            {
                cbSelloTiempo.Checked = true;
                txtUrl.Enabled = true;
                txtUsuario.Enabled = true;
                txtContrasenia.Enabled = true;
            }
            else
            {
                cbSelloTiempo.Checked = false;
                txtUrl.Enabled = false;
                txtUsuario.Enabled = false;
                txtContrasenia.Enabled = false;
            }
        }

        private void txtContrasenia_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
