﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AppSignFastDesktop
{
    public partial class frmVisorPDF : Form
    {
        private String RutaArchivoTemporal { get; set; }

        public frmVisorPDF(String rutaArchivoTemporal)
        {
            InitializeComponent();

            RutaArchivoTemporal = rutaArchivoTemporal;
        }

        private void frmVisorPDF_Load(object sender, EventArgs e)
        {
            axAcroPdfVisor.LoadFile(RutaArchivoTemporal);
        }

        private void frmVisorPDF_FormClosing(object sender, FormClosingEventArgs e)
        {
           
        }

        //private void frmVisorPdf_Load(object sender, EventArgs e)
        //{
        //    axAcroPdfVisor.LoadFile(RutaArchivoTemporal);
        //}

        //private void btnAceptar_Click(object sender, EventArgs e)
        //{
        //    Close();
        //}

        //private void frmVisorPdf_FormClosing(object sender, FormClosingEventArgs e)
        //{
        //    if (File.Exists(RutaArchivoTemporal))
        //    {
        //        File.Delete(RutaArchivoTemporal);
        //    }
        //}
    }
}
