﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using SignFastCore;
using Org.BouncyCastle.Pkcs;
using Org.BouncyCastle.Crypto.Parameters;
using Org.BouncyCastle.X509;
using iTextSharp.text.pdf;
using iTextSharp.text.pdf.security;
using iTextSharp.text;
using W = System.Security.Cryptography.X509Certificates;
using Org.BouncyCastle.Security;
using AppSignFastDesktop.Properties;
using com.itextpdf.text.pdf.security;
using iTextSharp.text.io;
using System.Security.Cryptography;
using Newtonsoft.Json.Linq;
using System.Net;
using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Math;
using System.Security.Cryptography.X509Certificates;
using Org.BouncyCastle.Asn1;
using Org.BouncyCastle.Asn1.X509;
using iTextSharp.text.error_messages;
using iTextSharp.text.exceptions;

namespace AppSignFastDesktop
{
    public partial class frmFirmar : Form
    {
        private const String Url_Servicio_Caducidad = "https://portal.smartboleta.com/SignFastServiceDesktop/services/rest/GestionarCaducidad/consultar/";

        private JObject jsonRespuesta;

        private SFCCertificado certificadoLocalSeleccionado;

        private frmConfigurarFirmaHolografica frmFirmaHolografica = new frmConfigurarFirmaHolografica();

        private frmValidacionDocumento frmValidacion = new frmValidacionDocumento();

        private frmConfigurarTimeStamp frmTimeStamp = new frmConfigurarTimeStamp();

        private frmConfiguracionSufijo frmSufijo = new frmConfiguracionSufijo();

        private frmConfiguracionCoordenadas frmCoordenadas = new frmConfiguracionCoordenadas();

        private frmClave frmClave = new frmClave();

        List<String> lstRutaArchivos = new List<string>();

        List<String> lstNombres = new List<string>();

        private Boolean certificadoSeleccionadoValido = false;

        private String mensajeValidacionCertificadoSeleccionado = "El certificado seleccionado no es válido.";

        private int posicionFilaDocumentoSeleccionado;

        private int Cancelar= 0;

        String ruta="";

        int num = 0;

        int documentosfirmados = 0;

        frmEsperaFirmando frmEsperando;

        private frmCaducidad frnCaducidad = new frmCaducidad();
        public frmFirmar()
        {
            InitializeComponent();
        }

        private void btnAñadir_Click(object sender, EventArgs e)
        {
            cargarPdf();
            MessageBox.Show("Se cargo correctamente los documentos.", "SignFastDesktop", MessageBoxButtons.OK, MessageBoxIcon.Information);

        }

        private void frmFirmar_Load(object sender, EventArgs e)
        {
            try
            {
                String urlServicioWeb = Url_Servicio_Caducidad + (string)Settings.Default["rucEmpresa"];

                jsonRespuesta = getDatosServicioWebRest(urlServicioWeb);

                String fechaFin = jsonRespuesta.GetValue("fechaFin").ToObject<String>();
                String fechaActual = jsonRespuesta.GetValue("fechaActual").ToObject<String>();

                DateTime fechaFinD = DateTime.Parse(fechaFin);
                DateTime fechaActualD = DateTime.Parse(fechaActual);

                if (fechaActualD >= fechaFinD)
                {
                    //MessageBox.Show("FECHAS : " + fechaActualD + "-" + fechaFinD);
                    frnCaducidad.ShowDialog();
                    //Application.Exit();
                }
                else
                {
                    string desktopPath = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile);
                    System.IO.Directory.CreateDirectory(desktopPath + "\\.SignFast");

                    using (frmMensajeCargando frm = new frmMensajeCargando(cargarCertificado, "Cargando certificados ..."))
                    {
                        frm.ShowDialog(this);
                    }
                }
            }
            catch(Exception excepcion)
            {
                MessageBox.Show("Error en validación de licencia. \n" + excepcion.Message, "SignFastDesktop", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                Application.Exit();
            }

    }

        void cargarPdf()
        {
            try
            {
                ofdAñadir.DefaultExt = "pdf";
                ofdAñadir.Filter = "pdf files (*.pdf)|*.pdf";
                ofdAñadir.Multiselect = true;

                int totalFilas = dgvListaDocumentos.RowCount;

                if (ofdAñadir.ShowDialog(this) == DialogResult.OK)
                {
                    foreach (String documentos in ofdAñadir.FileNames)
                    {
                        totalFilas++;
                        String ruta = Path.GetFullPath(documentos);
                        String nombreDocumento = Path.GetFileName(documentos);
                        dgvListaDocumentos.Rows.Add(totalFilas, nombreDocumento,ruta);
                        Console.WriteLine("Ruta " + ruta);
                        Console.WriteLine("Nombre Documento " +nombreDocumento);
                        lstRutaArchivos.Add(ruta);

                        if((int)Settings.Default["PermisoSufijo"] == 1)
                        {
                            nombreDocumento = nombreDocumento.Replace(".pdf", (string)Settings.Default["TextoSufijo"]) + ".pdf";
                            //MessageBox.Show(nombreDocumento);
                            lstNombres.Add(nombreDocumento);
                        }
                        else
                        {
                            lstNombres.Add(nombreDocumento);
                        }

                        



                    }
                }
            }
            catch (System.Exception excepcion)
            {
                throw excepcion;
            }
        }

        void cargarPdfDesdeCarpeta()
        {
            try
            {
                int totalFilas = dgvListaDocumentos.RowCount;

                if (fbdAñadirCarpeta.ShowDialog() == DialogResult.OK)
                {
                    String rutaCarpeta = fbdAñadirCarpeta.SelectedPath;
                    DirectoryInfo di = new DirectoryInfo(rutaCarpeta);

                    if(di.GetFiles() != null)
                    {
                        foreach (var documentos in di.GetFiles())
                        {                           
                            var ruta = documentos.FullName;
                            var nombreDocumento = documentos.Name;
                            if (ruta.Contains(".pdf") && nombreDocumento.Contains(".pdf"))
                            {
                                totalFilas++;
                                dgvListaDocumentos.Rows.Add(totalFilas, nombreDocumento,ruta);
                                lstRutaArchivos.Add(ruta);
                                //lstNombres.Add(nombreDocumento);
                                if ((int)Settings.Default["PermisoSufijo"] == 1)
                                {
                                    nombreDocumento = nombreDocumento.Replace(".pdf", (string)Settings.Default["TextoSufijo"]) +".pdf";
                                    //MessageBox.Show(nombreDocumento);
                                    lstNombres.Add(nombreDocumento);
                                }
                                else
                                {
                                    lstNombres.Add(nombreDocumento);
                                }
                            }                          
                        }
                    }

                    if(di.GetDirectories() != null)
                    {
                        foreach (var directorios in di.GetDirectories())
                        {
                            foreach (var documentos in directorios.GetFiles())
                            {                             
                                var ruta = documentos.FullName;
                                var nombreDocumento = documentos.Name;
                                if (ruta.Contains(".pdf") && nombreDocumento.Contains(".pdf"))
                                {
                                    totalFilas++;
                                    dgvListaDocumentos.Rows.Add(totalFilas, nombreDocumento);
                                    lstRutaArchivos.Add(ruta);
                                    //lstNombres.Add(nombreDocumento);
                                    if ((int)Settings.Default["PermisoSufijo"] == 1)
                                    {
                                        nombreDocumento = nombreDocumento.Replace(".pdf", (string)Settings.Default["TextoSufijo"]) + ".pdf";
                                        //MessageBox.Show(nombreDocumento);
                                        lstNombres.Add(nombreDocumento);
                                    }
                                    else
                                    {
                                        lstNombres.Add(nombreDocumento);
                                    }
                                }
                            }

                        }
                    }  
                }
            }
            catch(System.Exception excepcion)
            {
                throw excepcion;
            }
        }


        void seleccionarCarpetaDestino()
        {
            if (fbdDestino.ShowDialog() == DialogResult.OK)
            {
                txtDestino.Text = fbdDestino.SelectedPath;
            }
        }

        void cargarCertificado()
        {
            lstViewCertificados.Invoke(new Action(() => lstViewCertificados.Clear()));
            /** crear columna en lstViewCertificados */
            ColumnHeader columnaCertificado = new ColumnHeader();
            lstViewCertificados.Invoke(new Action(() => lstViewCertificados.Columns.Add(columnaCertificado)));

            ListViewItem item = null;
            List<SFCCertificado> listaCertificados = listarCertificadosBibliotecaWindows2();
            foreach (SFCCertificado certificado in listaCertificados)
            {
                if(!certificado.alias.ToLower().Contains("reniec class"))
                {
                    item = new ListViewItem();
                    item.Text = certificado.alias;
                    item.Tag = certificado;
                    item.ImageIndex = 0;

                    lstViewCertificados.Invoke(new Action(() => lstViewCertificados.Items.Add(item)));
                }
                

                
            }

            lstViewCertificados.Invoke(new Action(() => lstViewCertificados.Columns[0].AutoResize(ColumnHeaderAutoResizeStyle.ColumnContent)));
        }

        private List<SFCCertificado> listarCertificadosBibliotecaWindows2()
        {
            var listaCertificados = new List<SFCCertificado>();

            SFCCertificado sfcCertificado = null;

            W.X509Store store = new W.X509Store(W.StoreName.My);
            store.Open(W.OpenFlags.ReadOnly);

            foreach (W.X509Certificate2 certificado in store.Certificates)
            {
                if (certificado.HasPrivateKey)
                {
                    sfcCertificado = new SFCCertificado();
                    sfcCertificado.x509Certificate = certificado;
                    sfcCertificado.alias = obtenerAliasCertificado2(certificado);

                    Indecopi2017CertificadoVerificador certificadoVerificador = new Indecopi2017CertificadoVerificador(sfcCertificado);
                    certificadoVerificador.validarNoRepudio = true;
                    certificadoVerificador.validarCrl = false;
                    certificadoVerificador.validarTsl = false;
                    certificadoVerificador.validarTslDesdeArchivo = false;

                    if (certificadoVerificador.esValidoParaFirmar())
                    {
                        listaCertificados.Add(sfcCertificado);
                    }
                }
            }

            store.Close();

            return listaCertificados;

        }

        public static String obtenerAliasDeCertificadoDigital(SFCCertificado certificate)
        {

            String alias = "";
            String subject = certificate.x509Certificate.ToString();
            //String[] lista = subject.Split(',');
            var kvs = subject.Split(',').Select(x => new KeyValuePair<string, string>(x.Split('=')[0], x.Split('=')[1])).ToList();

            for (int i = 0; i < kvs.Count; i++)
            {
                if (kvs[i].Key.Contains("CN"))
                {
                    alias = kvs[i].Value;
                }
            }
            return alias;
        }

        public String obtenerAliasCertificado2(W.X509Certificate2 certificado)
        {
            if (!certificado.HasPrivateKey)
            {
                throw new System.ArgumentNullException("El atributo certificado está nulo");
            }

            //if (String.IsNullOrEmpty(certificado.FriendlyName))
            //{
            //    return certificado.GetNameInfo(W.X509NameType.SimpleName, false);
            //}

            //return certificado.FriendlyName;
            return certificado.GetNameInfo(W.X509NameType.SimpleName, false);

        }

        private void btnDestino_Click(object sender, EventArgs e)
        {
            seleccionarCarpetaDestino();
        }

        private void lstViewCertificados_Click(object sender, EventArgs e)
        {
            certificadoLocalSeleccionado = (SFCCertificado)lstViewCertificados.SelectedItems[0].Tag;
        }

        private void btnFirmarDocumentos_Click(object sender, EventArgs e)
        {

            validarDocumentosConMismoNombre();

            certificadoSeleccionadoValido = false;

            if (certificadoLocalSeleccionado == null)
            {
                MessageBox.Show("Debe seleccionar un certificado para firmar.", "SignFastDesktop", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            if (txtDestino.Text.Trim().Length == 0)
            {
                MessageBox.Show("Seleccione una ruta de destino.", "SignFastDesktop", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            int i = dgvListaDocumentos.RowCount;

            if (i == 0)
            {
                MessageBox.Show("No hay documentos por firmar.", "SignFastDesktop", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            using (frmEspera frm = new frmEspera(validarCertificadoSeleccionado, "Validando certificado seleccionado ..."))
            {
                frm.ShowDialog(this);
            }

            if (certificadoSeleccionadoValido == true)
            {


                if ((int)Settings.Default["PermisoSufijo"] == 1)
                {
                    DialogResult result = MessageBox.Show("Los documentos a firmar tendran una terminacion con el sufijo: " + (string)Settings.Default["TextoSufijo"] + ". " + "Desea Continuar?", "Alerta", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                    if (result == DialogResult.Yes)
                    {
                        using (frmEsperando = new frmEsperaFirmando(firmarDocumento, "Firmando documentos ...", dgvListaDocumentos.RowCount))
                        {
                            frmEsperando.ShowDialog(this);
                        }
                    }
                    else
                    {
                        return;
                    }
                }
                else if(num > 0)
                {
                    DialogResult result = MessageBox.Show("Es posible que algunos documentos sean sobreescritos. Desea continuar?", "Alerta", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                    if (result == DialogResult.Yes)
                    {
                        using (frmEsperando = new frmEsperaFirmando(firmarDocumento, "Firmando documentos ...", dgvListaDocumentos.RowCount))
                        {
                            frmEsperando.ShowDialog(this);
                        }
                    }
                    else
                    {
                        return;
                    }
                }
                else
                {
                    //using (frmEsperaFirmando frm = new frmEsperaFirmando(firmarDocumento, "Firmando documentos ...", dgvListaDocumentos.RowCount))
                    using (frmEsperando = new frmEsperaFirmando(firmarDocumento, "Firmando documentos ...", dgvListaDocumentos.RowCount))
                    {
                        frmEsperando.ShowDialog(this);
                    }
                }

                num = 0;
                if(Cancelar != 1)
                {
                    MessageBox.Show("Firma realizada exitosamente.", "SignFastDesktop", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dgvListaDocumentos.Rows.Clear();
                }
               
            }
            else
            {
                MessageBox.Show(mensajeValidacionCertificadoSeleccionado, "SignFastDesktop", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            //MessageBox.Show("Firma realizada exitosamente.", "SignFastDesktop", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //dgvListaDocumentos.Rows.Clear();
        }

        private void btnAñadirCarpeta_Click(object sender, EventArgs e)
        {
            cargarPdfDesdeCarpeta();
        }

        private void firmaHolográficaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmFirmaHolografica.ShowDialog();
        }

        private void btnQuitar_Click(object sender, EventArgs e)
        {
            int i = dgvListaDocumentos.RowCount;
 
            if(i == 0)
            {
               MessageBox.Show("Debe agregar al menos un documento.", "SignFastDesktop", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
            dgvListaDocumentos.Rows.RemoveAt(dgvListaDocumentos.CurrentRow.Index);
        }

        private void validacionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmValidacion.ShowDialog();
        }

        private void firmarDocumento()
        {
            //try
            //{
            //    Cancelar = 0;
            //    /** cargando certificado */
            //    ICollection<Org.BouncyCastle.X509.X509Certificate> chain = new List<Org.BouncyCastle.X509.X509Certificate>();       
            //    X509Chain chainn = new X509Chain();
            //    chainn.Build(certificadoLocalSeleccionado.x509Certificate);

            //    for (int i = 0; i < chainn.ChainElements.Count; i++)
            //    {
            //        Console.WriteLine(chainn.ChainElements[i].Certificate.Issuer);
            //        chain.Add(Org.BouncyCastle.Security.DotNetUtilities.FromX509Certificate(chainn.ChainElements[i].Certificate));
            //    }

            //    int filasArchivos = dgvListaDocumentos.RowCount;
            //    for (int posicionFiles = 0; posicionFiles < filasArchivos; posicionFiles++)
            //    {
            //        if (filasArchivos > 80)
            //        {
            //            frmEsperando.actualizarEtiqueta("Firmando " + (posicionFiles + 1) + " de " + (filasArchivos));
            //            System.Threading.Thread.Sleep(100);
            //        }
            //        else
            //        {
            //            frmEsperando.actualizarEtiqueta("Firmando " + (posicionFiles + 1) + " de " + (filasArchivos));
            //            System.Threading.Thread.Sleep(1000);

            //        }

            //        String rutaArchivo = lstRutaArchivos[posicionFiles];
            //        String nombreDocumentos = lstNombres[posicionFiles];

            //        String nombreFirma = "sig" + DateTime.Now.ToString("ddMMyyyyHHmmss");

            //        /** Configurando Pdf - inicio */
            //        PdfReader reader = new PdfReader(rutaArchivo);
            //        FileStream osFinal = new FileStream(txtDestino.Text + "\\" + nombreDocumentos, FileMode.Create);
            //        PdfStamper stamper = PdfStamper.CreateSignature(reader, osFinal, '\0');

            //        PdfSignatureAppearance appearance = stamper.SignatureAppearance;


            //        appearance.Reason = "Firma Digital - " + (string)Settings.Default["empresa"];
            //        appearance.Location = "Lima";

            //        if ((int)Settings.Default["PermisoFirmaHolografa"] == 1)
            //        {
            //            string rutaFirmaHolografica = Path.GetFullPath((string)Settings.Default["RutaImagen"]);
            //            StringBuilder sbTextoFirmaHolografica = new StringBuilder();
            //            sbTextoFirmaHolografica.Append("Firmado Digitalmente Por: " + certificadoLocalSeleccionado.alias);
            //            sbTextoFirmaHolografica.Append("\nMotivo: " + (string)Settings.Default["Motivo"]);
            //            sbTextoFirmaHolografica.Append("\nFecha: " + DateTime.Now);

            //            appearance.SignatureRenderingMode = PdfSignatureAppearance.RenderingMode.GRAPHIC_AND_DESCRIPTION;
            //            appearance.SignatureGraphic = iTextSharp.text.Image.GetInstance(File.ReadAllBytes(rutaFirmaHolografica));
            //            appearance.Layer2Text = sbTextoFirmaHolografica.ToString();
            //            int posicion = (int)Settings.Default["Posicion"];
            //            string pagina = (string)Settings.Default["Pagina"];
            //            int paginaSeleccionada = 0;

            //            if (pagina.Contains("PRIMERA"))
            //            {
            //                paginaSeleccionada = 1;
            //            }
            //            else
            //            {
            //                paginaSeleccionada = reader.NumberOfPages;
            //            }

            //            iTextSharp.text.Rectangle cropbox = reader.GetPageSize(1);
            //            iTextSharp.text.Rectangle rectangulo = new SignFastUtil().getStampRectangle(posicion, cropbox);

            //            appearance.SetVisibleSignature(rectangulo, paginaSeleccionada, nombreFirma);
            //        }
            //        else
            //        {
            //            iTextSharp.text.Rectangle rectanguloFirma = new iTextSharp.text.Rectangle(0, 0, 0, 0);
            //            appearance.SetVisibleSignature(rectanguloFirma, 1, nombreFirma);
            //        }

            //        /** Configurando Pdf - fin */

            //        IOcspClient ocspClient = new OcspClientBouncyCastle(new OcspVerifier(null, null));
            //        ICrlClient crlClient = new CrlClientOnline(chain);
            //        List<ICrlClient> listCrl = new List<ICrlClient>();
            //        listCrl.Add(crlClient);

            //        ITSAClient tsaClient = null;
            //        if ((int)Settings.Default["PermisoSelloTiempo"] == 1)
            //        {
            //            tsaClient = new TSAClientBouncyCastle((string)Settings.Default["Url"], (string)Settings.Default["Usuario"], (string)Settings.Default["Contrasenia"]);
            //        }

            //        var pk = Org.BouncyCastle.Security.DotNetUtilities.GetKeyPair(certificadoLocalSeleccionado.x509Certificate.PrivateKey).Private;
            //        IExternalSignature pks = new PrivateKeySignature(pk, DigestAlgorithms.SHA256);
            //        MakeSignature.SignDetached(appearance, pks, chain, listCrl, ocspClient, tsaClient, 55000, CryptoStandard.CMS);

            //        //pdfStamper.Close();
            //        //osFinal.Flush();

            //        //byte[] signedDocument = ms.ToArray();
            //        //osFinal.Close();
            //        //reader.Close();
            //        documentosfirmados++;

            //    }
            //    lstRutaArchivos.Clear();
            //    lstNombres.Clear();

            //}
            //catch (System.Exception excepcion)
            //{
            //    Cancelar = 1;
            //    MessageBox.Show(excepcion.Message, "SignFastDesktop", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            //    Console.WriteLine(excepcion.StackTrace);

            //}
            try
            {
                if (certificadoLocalSeleccionado.alias.Contains("hard") && certificadoLocalSeleccionado.alias.Contains("FIR"))
                {
                    Console.WriteLine("Se firmara con dniE");

                    Cancelar = 0;
                    /** cargando certificado */
                    ICollection<Org.BouncyCastle.X509.X509Certificate> chain = new List<Org.BouncyCastle.X509.X509Certificate>();
                    chain.Add(Org.BouncyCastle.Security.DotNetUtilities.FromX509Certificate(certificadoLocalSeleccionado.x509Certificate));

                    Console.WriteLine(certificadoLocalSeleccionado.alias);


                    int filasArchivos = dgvListaDocumentos.RowCount;
                    for (int posicionFiles = 0; posicionFiles < filasArchivos; posicionFiles++)
                    {
                        /** Eliminar */
                        frmEsperando.actualizarEtiqueta("Firmando " + (posicionFiles + 1) + " de " + (filasArchivos));
                        System.Threading.Thread.Sleep(3000);
                        if (filasArchivos > 80)
                        {
                            frmEsperando.actualizarEtiqueta("Firmando " + (posicionFiles + 1) + " de " + (filasArchivos));
                            System.Threading.Thread.Sleep(500);
                        }
                        else
                        {
                            frmEsperando.actualizarEtiqueta("Firmando " + (posicionFiles + 1) + " de " + (filasArchivos));
                            System.Threading.Thread.Sleep(1000);

                        }

                        String rutaArchivo = lstRutaArchivos[posicionFiles];
                        String nombreDocumentos = lstNombres[posicionFiles];

                        String nombreFirma = "sig" + DateTime.Now.ToString("ddMMyyyyHHmmss");

                        /** Configurando Pdf - inicio */
                        PdfReader reader = new PdfReader(rutaArchivo);
                        MemoryStream os = new MemoryStream();
                        PdfStamper stamper = PdfStamper.CreateSignature(reader, os, '\0', null, true);

                        PdfSignatureAppearance appearance = stamper.SignatureAppearance;


                        appearance.Reason = "Firma Digital - " + (string)Settings.Default["empresa"];
                        appearance.Location = "Lima";

                        if ((int)Settings.Default["PermisoFirmaHolografa"] == 1)
                        {
                            appearance.Reason = (string)Settings.Default["Motivo"];

                            string rutaFirmaHolografica = Path.GetFullPath((string)Settings.Default["RutaImagen"]);
                            StringBuilder sbTextoFirmaHolografica = new StringBuilder();
                            sbTextoFirmaHolografica.Append("Firmado Digitalmente Por: " + certificadoLocalSeleccionado.alias);
                            //sbTextoFirmaHolografica.Append("\nCargo: " + (string)Settings.Default["Cargo"]);
                            sbTextoFirmaHolografica.Append("\nMotivo: " + (string)Settings.Default["Motivo"]);
                            sbTextoFirmaHolografica.Append("\nFecha: " + DateTime.Now);

                            appearance.SignatureRenderingMode = PdfSignatureAppearance.RenderingMode.GRAPHIC_AND_DESCRIPTION;
                            appearance.SignatureGraphic = iTextSharp.text.Image.GetInstance(File.ReadAllBytes(rutaFirmaHolografica));
                            appearance.Layer2Text = sbTextoFirmaHolografica.ToString();
                            int posicion = (int)Settings.Default["Posicion"];
                            string pagina = (string)Settings.Default["Pagina"];
                            int ancho = (int)Settings.Default["Ancho"];
                            int alto = (int)Settings.Default["Alto"];
                            int paginaSeleccionada = 0;

                            if (pagina.Contains("PRIMERA"))
                            {
                                paginaSeleccionada = 1;
                            }
                            else
                            {
                                paginaSeleccionada = reader.NumberOfPages;
                            }

                            iTextSharp.text.Rectangle cropbox = reader.GetPageSize(1);
                            iTextSharp.text.Rectangle rectangulo = new SignFastUtil().getStampRectangle(posicion, cropbox,ancho,alto);

                            appearance.SetVisibleSignature(rectangulo, paginaSeleccionada, nombreFirma);
                        }
                        else
                        {
                            iTextSharp.text.Rectangle rectanguloFirma = new iTextSharp.text.Rectangle(0, 0, 0, 0);
                            appearance.SetVisibleSignature(rectanguloFirma, 1, nombreFirma);
                        }

                        /** Configurando Pdf - fin */

                        /** Firmando externamente pdf - inicio */
                        IExternalSignatureContainer external = new ExternalBlankSignatureContainer(PdfName.ADOBE_PPKLITE, PdfName.ADBE_PKCS7_DETACHED);
                        MakeSignature.SignExternalContainer(appearance, external, 8192);
                        /** Firmando externamente pdf - fin */

                        /** obteniendo bytes de la firma externa pdf - inicio */
                        byte[] bytesPdfFirmadoTemporal = os.ToArray();
                        /** obteniendo bytes de la firma externa pdf - fin */

                        /** obteniendo hash de documento */
                        PdfReader readerTmp = new PdfReader(new RandomAccessFileOrArray(bytesPdfFirmadoTemporal), null); ;
                        AcroFields acroFields = readerTmp.AcroFields;
                        PdfDictionary pdfDictionary = acroFields.GetSignatureDictionary(nombreFirma);

                        if (pdfDictionary == null)
                        {
                            throw new iTextSharp.text.DocumentException("No existe ningun campo con el nombre: " + nombreFirma);
                        }

                        if (!acroFields.SignatureCoversWholeDocument(nombreFirma))
                        {
                            throw new iTextSharp.text.DocumentException("Not the last signature");
                        }

                        PdfArray pdfArray = pdfDictionary.GetAsArray(PdfName.BYTERANGE);
                        long[] gaps = pdfArray.AsLongArray();
                        if (pdfArray.Size != 4 || gaps[0] != 0)
                        {
                            throw new iTextSharp.text.DocumentException("Single exclusion space supported");
                        }

                        IRandomAccessSource readerSource = readerTmp.SafeFile.CreateSourceView();
                        //RASInputStream rASInputStream = new iTextSharp.text.io.RASInputStream(readerSource);
                        RASInputStream rASInputStream = new RASInputStream(new RandomAccessSourceFactory().CreateRanged(readerSource, gaps));

                        PdfPKCS7 pdfPkcs7 = new PdfPKCS7(null, chain, "SHA256", false);
                        //OID DEL ALGORTIMO SHA256 -> "2.16.840.1.101.3.4.2.1"
                        byte[] digest = DigestAlgorithms.Digest(rASInputStream, "2.16.840.1.101.3.4.2.1");
                        byte[] hash = pdfPkcs7.getAuthenticatedAttributeBytes(digest, null, null, CryptoStandard.CMS);

                        readerTmp.Close();
                        os.Close();

                        /** firmando hash */
                        byte[] signature = null;
                        RSACryptoServiceProvider rsaAesCSP = null;
                        RSACryptoServiceProvider privateKey = null;
                        privateKey = certificadoLocalSeleccionado.x509Certificate.PrivateKey as RSACryptoServiceProvider;

                        if (privateKey != null)
                        {
                            if (!privateKey.CspKeyContainerInfo.HardwareDevice)
                            {
                                CspParameters cspParameters = new CspParameters();
                                cspParameters.KeyContainerName = privateKey.CspKeyContainerInfo.KeyContainerName;
                                cspParameters.KeyNumber = privateKey.CspKeyContainerInfo.KeyNumber == KeyNumber.Exchange ? 1 : 2;

                                rsaAesCSP = new RSACryptoServiceProvider(cspParameters);
                                rsaAesCSP.PersistKeyInCsp = true;
                            }
                            else
                            {
                                rsaAesCSP = privateKey;
                            }

                            signature = rsaAesCSP.SignData(hash, new SHA256Managed());

                        }


                        /** incrustar firma digital - inicio */
                        PdfReader readerFinal = new PdfReader(bytesPdfFirmadoTemporal);
                        IExternalSignatureContainer externalFinal = new SignFastCoreSignatureContainer(chain, digest, signature);
                        FileStream osFinal = new FileStream(txtDestino.Text + "\\" + nombreDocumentos, FileMode.Create);
                        MakeSignature.SignDeferred(readerFinal, nombreFirma, osFinal, externalFinal);
                        /** incrustar firma digital - fin */

                        osFinal.Close();
                        readerFinal.Close();
                        documentosfirmados++;


                    }
                    lstRutaArchivos.Clear();
                    lstNombres.Clear();
                }
                else
                {
                    Console.WriteLine("Se firmara con certificado archivo");
                    Cancelar = 0;
                    /** cargando certificado */
                    ICollection<Org.BouncyCastle.X509.X509Certificate> chain = new List<Org.BouncyCastle.X509.X509Certificate>();
                    X509Chain chainn = new X509Chain();
                    chainn.Build(certificadoLocalSeleccionado.x509Certificate);

                    for (int i = 0; i < chainn.ChainElements.Count; i++)
                    {
                        Console.WriteLine(chainn.ChainElements[i].Certificate.Issuer);
                        chain.Add(Org.BouncyCastle.Security.DotNetUtilities.FromX509Certificate(chainn.ChainElements[i].Certificate));
                    }

                    int filasArchivos = dgvListaDocumentos.RowCount;
                    for (int posicionFiles = 0; posicionFiles < filasArchivos; posicionFiles++)
                    {
                        if (filasArchivos > 80)
                        {
                            frmEsperando.actualizarEtiqueta("Firmando " + (posicionFiles + 1) + " de " + (filasArchivos));
                            System.Threading.Thread.Sleep(100);
                        }
                        else
                        {
                            frmEsperando.actualizarEtiqueta("Firmando " + (posicionFiles + 1) + " de " + (filasArchivos));
                            System.Threading.Thread.Sleep(1000);

                        }

                        String rutaArchivo = lstRutaArchivos[posicionFiles];
                        String nombreDocumentos = lstNombres[posicionFiles];

                        String nombreFirma = "sig" + DateTime.Now.ToString("ddMMyyyyHHmmss");

                        /** Configurando Pdf - inicio */

                        bool tienePass = IsPasswordProtected(rutaArchivo);

                        PdfReader reader;
                        if (tienePass)
                        {
                            frmClave.ShowDialog();
                            String clave = frmClave.getText();
                            byte[] passwordBytes = Encoding.ASCII.GetBytes(clave);
                            reader = new PdfReader(rutaArchivo, passwordBytes);
                        }
                        else
                        {
                            reader = new PdfReader(rutaArchivo);
                        }

                        //PdfReader reader = new PdfReader(rutaArchivo);
                        //RandomAccessFileOrArray f = new RandomAccessFileOrArray(rutaArchivo);
                        //PdfReader reader = new PdfReader(f, null);

                        FileStream osFinal = new FileStream(txtDestino.Text + "\\" + nombreDocumentos, FileMode.Create);
                        PdfStamper stamper = PdfStamper.CreateSignature(reader, osFinal, '\0',null, true);

                        PdfSignatureAppearance appearance = stamper.SignatureAppearance;


                        appearance.Reason = "Firma Digital - " + (string)Settings.Default["empresa"];
                        appearance.Location = "Lima";

                        if ((int)Settings.Default["PermisoFirmaHolografa"] == 1)
                        {
                            string cargo = (string)Settings.Default["Cargo"];
                            string motivo = (string)Settings.Default["Motivo"];
                            string dni = (string)Settings.Default["Dni"];

                            string rutaFirmaHolografica = (string)Settings.Default["RutaImagen"];
                            StringBuilder sbTextoFirmaHolografica = new StringBuilder();
                            sbTextoFirmaHolografica.Append("Firmado digitalmente por: " + certificadoLocalSeleccionado.alias);
                            sbTextoFirmaHolografica.Append("\nMotivo: " + (string)Settings.Default["Motivo"]);
                            sbTextoFirmaHolografica.Append("\nFecha: " + DateTime.Now);

                            if (!rutaFirmaHolografica.Equals(""))
                            {
                                rutaFirmaHolografica = Path.GetFullPath(rutaFirmaHolografica);
                                appearance.SignatureRenderingMode = PdfSignatureAppearance.RenderingMode.GRAPHIC_AND_DESCRIPTION;
                                appearance.SignatureGraphic = iTextSharp.text.Image.GetInstance(File.ReadAllBytes(rutaFirmaHolografica));
                            }
                            else
                            {
                                appearance.SignatureRenderingMode = PdfSignatureAppearance.RenderingMode.DESCRIPTION;
                            }
                            
                            appearance.Layer2Text = sbTextoFirmaHolografica.ToString();
                            int posicion = (int)Settings.Default["Posicion"];
                            string pagina = (string)Settings.Default["Pagina"];
                            int posicionX = (int)Settings.Default["X"];
                            int posicionY = (int)Settings.Default["Y"];
                            int ancho = (int)Settings.Default["Ancho"];
                            int alto = (int)Settings.Default["Alto"];
                            int paginaSeleccionada = 0;

                            if (pagina.Contains("PRIMERA"))
                            {
                                paginaSeleccionada = 1;
                            }
                            else
                            {
                                paginaSeleccionada = reader.NumberOfPages;
                            }

                            if((int)Settings.Default["PermisoPosiciones"] == 1)
                            {
                                iTextSharp.text.Rectangle cropbox = reader.GetPageSize(1);
                                iTextSharp.text.Rectangle rectangulo = new SignFastUtil().getStampRectangle(posicion, cropbox, ancho, alto);
                                appearance.SetVisibleSignature(rectangulo, paginaSeleccionada, nombreFirma);

                            }
                            else
                            {
                                iTextSharp.text.Rectangle rectangulo = new iTextSharp.text.Rectangle(posicionX, posicionY, ancho + posicionX, alto + posicionY);
                                appearance.SetVisibleSignature(rectangulo, paginaSeleccionada, nombreFirma);

                            }


                        }
                        else
                        {
                            iTextSharp.text.Rectangle rectanguloFirma = new iTextSharp.text.Rectangle(0, 0, 0, 0);
                            appearance.SetVisibleSignature(rectanguloFirma, 1, nombreFirma);
                        }

                        /** Configurando Pdf - fin */

                        IOcspClient ocspClient = new OcspClientBouncyCastle(new OcspVerifier(null, null));
                        ICrlClient crlClient = new CrlClientOnline(chain);
                        List<ICrlClient> listCrl = new List<ICrlClient>();
                        listCrl.Add(crlClient);

                        ITSAClient tsaClient = null;
                        if ((int)Settings.Default["PermisoSelloTiempo"] == 1)
                        {
                            tsaClient = new TSAClientBouncyCastle((string)Settings.Default["Url"], (string)Settings.Default["Usuario"], (string)Settings.Default["Contrasenia"]);
                        }
                        RsaPrivateCrtKeyParameters parameters = Org.BouncyCastle.Security.DotNetUtilities.GetKeyPair(certificadoLocalSeleccionado.x509Certificate.PrivateKey).Private as RsaPrivateCrtKeyParameters;
                        //var pk = Org.BouncyCastle.Security.DotNetUtilities.GetKeyPair(certificadoLocalSeleccionado.x509Certificate.PrivateKey).Private;
                        IExternalSignature pks = new PrivateKeySignature(parameters, DigestAlgorithms.SHA256);
                        MakeSignature.SignDetached(appearance, pks, chain, listCrl, ocspClient, tsaClient, 55000, CryptoStandard.CMS);

                        osFinal.Close();
                        reader.Close();
                        //pdfStamper.Close();
                        //osFinal.Flush();

                        //byte[] signedDocument = ms.ToArray();
                        //osFinal.Close();
                        //reader.Close();
                        documentosfirmados++;

                    }
                    lstRutaArchivos.Clear();
                    lstNombres.Clear();
                }
            }
            catch (BadPasswordException passExcepcion)
            {
                Cancelar = 1;
                MessageBox.Show("Clave ingresada incorrecta", "SignFastDesktop", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                Console.WriteLine(passExcepcion.StackTrace);
            }
            catch (System.Exception excepcion)
            {
                Cancelar = 1;
                MessageBox.Show(excepcion.Message, "SignFastDesktop", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                Console.WriteLine(excepcion.StackTrace);
            }

        }

        private void validarCertificadoSeleccionado()
        {
            String rutaTemporalFicheroTsl = System.IO.Path.GetTempPath() + System.IO.Path.DirectorySeparatorChar + "tsl-pe.xml";

            try
            {
                Indecopi2017CertificadoVerificador certificadoVerificador = new Indecopi2017CertificadoVerificador(certificadoLocalSeleccionado);


                certificadoVerificador.urlTsl = Settings.Default["urlTsl"].ToString();
                certificadoVerificador.rutaArchivoTsl = rutaTemporalFicheroTsl;
                if ((int)Settings.Default["validarTsl"] == 1)
                {
                    certificadoVerificador.validarTsl = true;
                }
                else
                {
                    certificadoVerificador.validarTsl = false;
                }

                if ((int)Settings.Default["validarTslDesdeArchivo"] == 1)
                {
                    certificadoVerificador.validarTslDesdeArchivo = true;
                }
                else{
                    certificadoVerificador.validarTslDesdeArchivo = true;
                }

                    
                //certificadoVerificador.validarTsl = false;
                

                certificadoVerificador.validarNoRepudio = false;
               
                certificadoVerificador.validarCrl = false;
               

                //Indecopi2017CertificadoVerificador certificadoVerificador = new Indecopi2017CertificadoVerificador(certificadoLocalSeleccionado);
                //certificadoVerificador.validarTslDesdeArchivo = true;
                //certificadoVerificador.rutaArchivoTsl = rutaTemporalFicheroTsl;

                if (certificadoVerificador.esValidoParaFirmar())
                {
                    certificadoSeleccionadoValido = true;
                }
                else
                {
                    certificadoSeleccionadoValido = false;
                    mensajeValidacionCertificadoSeleccionado = certificadoVerificador.obtenerMensajeRespuestaVerificacion();
                }
            }
            catch (System.Exception excepcion)
            {
                MessageBox.Show("Ha ocurrido un error: " + excepcion.StackTrace);
            }
        }

        private void timeStampToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmTimeStamp.ShowDialog();
        }

        private void lblAddDocumentosIndividualmente_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            cargarPdf();
        }

        private void lblAddDocumentosPorCarpeta_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            cargarPdfDesdeCarpeta();
        }

        private void salirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        void mostrarDocumentoPdf()
        {
            //GestionarDocumentoClienteClient wsGestionarSignFastServer = new GestionarDocumentoClienteClient();
            try
            {
                ruta = dgvListaDocumentos.Rows[posicionFilaDocumentoSeleccionado].Cells[2].Value.ToString();

                byte[] content = File.ReadAllBytes(ruta);

                File.WriteAllBytes(ruta, content);
            }
            catch (System.Exception excepcion)
            {
                Console.WriteLine(excepcion.Message);
                Console.WriteLine(excepcion.StackTrace);
            }

        }

        private void dgvListaDocumentos_DoubleClick(object sender, EventArgs e)
        {
            //posicionFilaDocumentoSeleccionado = e.RowIndex;
            
            using (frmEspera frm = new frmEspera(mostrarDocumentoPdf, "Cargando visor PDF ..."))
            {
        
                frm.ShowDialog(this);
            }

            if (ruta == "")
            {
                MessageBox.Show("No hay documentos por visualizar.", "SignFastDesktop", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            if(dgvListaDocumentos.RowCount == 0)
            {
                MessageBox.Show("No hay documentos por visualizar.", "SignFastDesktop", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
            
            frmVisorPDF frmVisorPdf = new frmVisorPDF(ruta);
            frmVisorPdf.ShowDialog();

     
        }

        private void btnQuitarTodos_Click(object sender, EventArgs e)
        {
           while(dgvListaDocumentos.RowCount > 0)
            {
                dgvListaDocumentos.Rows.Remove(dgvListaDocumentos.CurrentRow);
            }
            lstNombres.Clear();
            lstRutaArchivos.Clear();
        }

        private void configuracionSufijoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmSufijo.ShowDialog();
        }

        void validarDocumentosConMismoNombre()
        {

            int filasArchivos = dgvListaDocumentos.RowCount;
            for (int posicionFiles = 0; posicionFiles < filasArchivos; posicionFiles++)
            {
                string nombreDocumento = lstNombres[posicionFiles];


                if (File.Exists(txtDestino.Text + "\\" + nombreDocumento))
                {
                    num++;
                }
            }
        }

        private void btnActualizarCertificados_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            using (frmMensajeCargando frm = new frmMensajeCargando(cargarCertificado, "Cargando certificados ..."))
            {
                frm.ShowDialog(this);
            }
        }

        private JObject getDatosServicioWebRest(String url)
        {
            JObject json = null;
            try
            {
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                var request = (HttpWebRequest)WebRequest.Create(url);
                request.Method = "GET";
                request.UserAgent = "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36";

                request.AutomaticDecompression = DecompressionMethods.Deflate | DecompressionMethods.GZip;
                var response = (HttpWebResponse)request.GetResponse();
                string content = string.Empty;
                using (var stream = response.GetResponseStream())
                {
                    using (var sr = new StreamReader(stream))
                    {
                        content = sr.ReadToEnd();
                    }
                }

                json = JObject.Parse(content);

            }
            catch (Exception excepcion)
            {
                //MessageBox.Show(excepcion.Message);
                MessageBox.Show("No se puede acceder a la validacion el linea. \n" + excepcion.Message, "SignFastDesktop", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                Application.Exit();
            }

            return json;
        }

        //public void firmar()
        //{
        //    /** Firma digital */
        //    const String src = "C:\\Users\\Carlos Altamirano\\Downloads\\ositran\\qr\\formato-ositran\\formato_ositran.pdf";
        //    const String dest = "C:\\Users\\Carlos Altamirano\\Downloads\\ositran\\qr\\formato-ositran\\formato_ositran[CSharp].pdf";

        //    try
        //    {
        //        /** capturar certificado digital */


        //        //Pkcs12Store store = new Pkcs12Store(new FileStream(@"D:\INDIGITALCERTS\COMPANY\ALRESA\ALRESA.pfx", FileMode.Open), "ALRESA".ToCharArray());
        //        Pkcs12Store store = new Pkcs12Store(new FileStream(@"D:\INDIGITALCERTS\COMPANY\CANTAGAS\INVERSIONES CANTA GAS SAC.p12", FileMode.Open), "EdiCANTAGAS16".ToCharArray());
        //        String alias = "";
        //        ICollection<X509Certificate> chain = new List<X509Certificate>();

        //        foreach (string al in store.Aliases)
        //        {
        //            if (store.IsKeyEntry(al) && store.GetKey(al).Key.IsPrivate)
        //            {
        //                alias = al;
        //                break;
        //            }
        //        }

        //        AsymmetricKeyEntry pk = store.GetKey(alias);
        //        foreach (X509CertificateEntry c in store.GetCertificateChain(alias))
        //        {
        //            chain.Add(c.Certificate);
        //        }

        //         RsaPrivateCrtKeyParameters parameters = pk.Key as RsaPrivateCrtKeyParameters;

        //        //var bcKey = DotNetUtilities.GetKeyPair(certificadoLocalSeleccionado.x509Certificate.PrivateKey);

        //        // DirectoryInfo di = new DirectoryInfo();

        //        int filasArchivos = dgvListaDocumentos.RowCount;

        //        for (int i = 0; i < filasArchivos; i++)
        //        {
        //            String rutaArchivo = lstRutaArchivos[i];
        //            String nombreDocumentos = lstNombres[i];



        //            PdfReader reader = new PdfReader(rutaArchivo);
        //            FileStream os = new FileStream(txtDestino.Text + "\\" + nombreDocumentos, FileMode.Create);

        //            PdfStamper stamper = PdfStamper.CreateSignature(reader, os, '\0',null,true);

        //            // Creating the appearance
        //            PdfSignatureAppearance appearance = stamper.SignatureAppearance;
        //            if (cbFirmaHolografica.Checked)
        //            {
        //                string rutaFirmaHolografica = Path.GetFullPath((string)Settings.Default["RutaImagen"]);

        //                StringBuilder sbTextoFirmaHolografica = new StringBuilder();
        //                sbTextoFirmaHolografica.Append("Firmado Digitalmente Por: " + alias);
        //                sbTextoFirmaHolografica.Append("\nMotivo: " + (string)Settings.Default["Motivo"]);
        //                sbTextoFirmaHolografica.Append("\nFecha: " + DateTime.Now);

        //                appearance.SignatureRenderingMode = PdfSignatureAppearance.RenderingMode.GRAPHIC_AND_DESCRIPTION; 
        //                appearance.SignatureGraphic = iTextSharp.text.Image.GetInstance(File.ReadAllBytes(rutaFirmaHolografica));

        //                appearance.Layer2Text = sbTextoFirmaHolografica.ToString();

        //                int posicion = (int)Settings.Default["Posicion"];
        //                string pagina = (string)Settings.Default["Pagina"];
        //                int paginaSeleccionada=0;

        //                if (pagina.Contains("PRIMERA"))
        //                {
        //                    paginaSeleccionada = 1;
        //                }
        //                else
        //                {
        //                    paginaSeleccionada = reader.NumberOfPages;
        //                }

        //                iTextSharp.text.Rectangle cropbox = reader.GetPageSize(1);
        //                iTextSharp.text.Rectangle rectangulo = new SignFastUtil().getStampRectangle(posicion,cropbox);

        //                appearance.SetVisibleSignature(rectangulo, paginaSeleccionada, "sig2");
        //            }
        //            //firmando...

        //            IExternalSignature pks = new PrivateKeySignature(parameters, DigestAlgorithms.SHA256);
        //            MakeSignature.SignDetached(appearance, pks, chain, null, null, null, 0, CryptoStandard.CMS);
        //        }

        //        //txtMensaje.Text = "PDF FIRMADO";
        //    }
        //    catch (System.Exception excepcion)
        //    {
        //        MessageBox.Show(excepcion.Message);
        //    }
        //}

        public static AsymmetricKeyParameter TransformRSAPrivateKey(AsymmetricAlgorithm privateKey)
        {
            RSACryptoServiceProvider prov = privateKey as RSACryptoServiceProvider;
            RSAParameters parameters = prov.ExportParameters(true);

            return new RsaPrivateCrtKeyParameters(
                new BigInteger(1, parameters.Modulus),
                new BigInteger(1, parameters.Exponent),
                new BigInteger(1, parameters.D),
                new BigInteger(1, parameters.P),
                new BigInteger(1, parameters.Q),
                new BigInteger(1, parameters.DP),
                new BigInteger(1, parameters.DQ),
                new BigInteger(1, parameters.InverseQ));
        }

        private static int calculateEstimatedSize(ICrlClient[] chain, IOcspClient ocspClient,
             ITSAClient tsaClient, ICollection<ICrlClient> crlList, int estimatedSizeBonus) {

         int estimatedSize = 0;
        ICollection<byte[]> crlBytes = null;
        int i = 0;
	        while (crlBytes == null && i<chain.Length)

                crlBytes = processCrl(chain[i++], crlList);
	    	if (estimatedSize == 0) {
	            estimatedSize = 8192;
	            if (crlBytes != null) {
	                foreach (byte[] element in crlBytes) {
	                    estimatedSize += element.Length + 10;
	                }
}
	            if (ocspClient != null)
	                estimatedSize += 4192;
	            if (tsaClient != null)
	                estimatedSize += 4192;
	        }
	    	//TODO Not enough space bug
	    	// adding 4192 by default
	    	if (estimatedSizeBonus == 0)
	    		estimatedSizeBonus = 4192;
	    	
	    	estimatedSize += estimatedSizeBonus;
	    	
	    	//logger.info("[SignUtil] calculateEstimatedSize() bonus: {} calculated: {}", estimatedSizeBonus, estimatedSize);
	    	return estimatedSize;
	 }
		
	 public static ICollection<byte[]> processCrl(ICrlClient cert, ICollection<ICrlClient> crlList)
     {
        if (crlList == null)
            return null;
        List<byte[]> crlBytes = new List<byte[]>();
        foreach (ICrlClient cc in crlList)
        {
        if (cc == null)
            continue;
        //logger.info("Processing " + cc.getClass().getName());
        ICollection<byte[]> b = cc.GetEncoded((Org.BouncyCastle.X509.X509Certificate)cert, null);
        if (b == null)
            continue;
            crlBytes.Union(b).ToList();
        }
        if (!crlBytes.Any())
            return null;
        else
            return crlBytes;
        }

        private void CoordenadasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmCoordenadas.ShowDialog();
        }

        public static bool IsPasswordProtected(string pdfFullname)
        {
            try
            {
                PdfReader pdfReader = new PdfReader(pdfFullname);
                return false;
            }
            catch (BadPasswordException)
            {
                return true;
            }
        }
    }
}
