﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AppSignFastDesktop
{
    public partial class frmEsperaFirmando : Form
    {
        public Action Worker { get; set; }

        int totalDocumentosPDF = 0;

        public frmEsperaFirmando(Action worker, String mensajeEspera, int totalDocumentos)
        {
            InitializeComponent();
            if (worker == null)
            {
                throw new ArgumentNullException();
            }

            Worker = worker;
            lblMensajeEspera.Text = mensajeEspera;
            //lblTotal.Text = totalDocumentos.ToString();
            totalDocumentosPDF = totalDocumentos;

            //for (int posicionFiles = 0; posicionFiles < totalDocumentosPDF; posicionFiles++)
            //{
            //    lblProcesados.Refresh();
            //    lblProcesados.Text = posicionFiles.ToString();
            //}

            //lblProcesados.Text = procesados.ToString();

        }
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            Task.Factory.StartNew(Worker).ContinueWith(t => { this.Close(); }, TaskScheduler.FromCurrentSynchronizationContext());
        }

     

        private void lblProcesados_TextChanged(object sender, EventArgs e)
        {
            //for (int posicionFiles = 0; posicionFiles < totalDocumentosPDF; posicionFiles++)
            //{
            //    lblProcesados.Text = posicionFiles.ToString();
            //    lblProcesados.Refresh();
                
            //}
        }

        public void actualizarEtiqueta(string newText)
        {
            this.Invoke(new MethodInvoker(() => lblMensajeEspera.Text = newText));
        }

        private void frmEsperaFirmando_Load(object sender, EventArgs e)
        {

        }
    }
}
