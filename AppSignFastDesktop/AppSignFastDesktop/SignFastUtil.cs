﻿using iTextSharp.text;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppSignFastDesktop
{
    public class SignFastUtil
    {
        public Rectangle getStampRectangle(int posicion, Rectangle cropBox, int ancho, int alto)
        {
            Rectangle rectangle = null;

            float width = ancho;//168
            float height = alto;//64

            switch (posicion)
            {
                case 1:
                    rectangle = new Rectangle(cropBox.Left + 30, cropBox.GetTop(height) - 30,
                            cropBox.GetLeft(width) + 30, cropBox.Top - 30);
                    break;
                case 2:
                    rectangle = new Rectangle(cropBox.Left + 200, cropBox.GetTop(height) - 30,
                            cropBox.GetLeft(width) + 200, cropBox.Top - 30);
                    break;
                case 3:
                    rectangle = new Rectangle(cropBox.GetRight(width) - 30, cropBox.GetTop(height) - 30,
                            cropBox.Right - 30, cropBox.Top - 30);
                    break;
                case 4:
                    rectangle = new Rectangle(cropBox.Left + 30, (cropBox.Height / 2) + (cropBox.Height / 4),
                            cropBox.GetLeft(width) + 30, (cropBox.Height / 2) + (cropBox.Height / 4) + 30);
                    break;
                case 5:
                    rectangle = new Rectangle(cropBox.Left + 200, (cropBox.Height / 2) + (cropBox.Height / 4),
                            cropBox.GetLeft(width) + 200, (cropBox.Height / 2) + (cropBox.Height / 4) + 30);
                    break;
                case 6:
                    rectangle = new Rectangle(cropBox.GetRight(width) - 30, (cropBox.Height / 2) + (cropBox.Height / 4),
                            cropBox.Right - 30, (cropBox.Height / 2) + (cropBox.Height / 4) + 30);
                    break;
                case 7:
                    rectangle = new Rectangle(cropBox.Left + 30, cropBox.Height / 2,
                            cropBox.GetLeft(width) + 30, (cropBox.Height / 2) + 30);
                    break;
                case 8:
                    rectangle = new Rectangle(cropBox.Left + 200, cropBox.Height / 2,
                            cropBox.GetLeft(width) + 200, (cropBox.Height / 2) + 30);
                    break;
                case 9:
                    rectangle = new Rectangle(cropBox.GetRight(width) - 30, cropBox.Height / 2,
                            cropBox.Right - 30, (cropBox.Height / 2) + 30);
                    break;
                case 10:
                    rectangle = new Rectangle(cropBox.Left + 30, cropBox.Height / 4,
                            cropBox.GetLeft(width) + 30, (cropBox.Height / 4) + 30);
                    break;
                case 11:
                    rectangle = new Rectangle(cropBox.Left + 200, cropBox.Height / 4,
                            cropBox.GetLeft(width) + 200, (cropBox.Height / 4) + 30);
                    break;
                case 12:
                    rectangle = new Rectangle(cropBox.GetRight(width) - 30, cropBox.Height / 4,
                            cropBox.Right - 30, (cropBox.Height / 4) + 30);
                    break;
                case 13:
                    rectangle = new Rectangle(cropBox.Left + 30, cropBox.Bottom + 30,
                            cropBox.GetLeft(width) + 30, cropBox.GetBottom(height) + 30);
                    break;
                case 14:
                    rectangle = new Rectangle(cropBox.Left + 200, cropBox.Bottom + 30,
                            cropBox.GetLeft(width) + 200, cropBox.GetBottom(height) + 30);
                    break;
                case 15:
                    rectangle = new Rectangle(cropBox.GetRight(width) - 30, cropBox.Bottom + 30,/*sube aleja*/
                            cropBox.Right - 30/*derecha aleja*/, cropBox.GetBottom(height) + 30);
                    break;
            }

            return rectangle;
        }
    }
}
