﻿using AppSignFastDesktop.Properties;
using iTextSharp.text.pdf;
using iTextSharp.text.pdf.security;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppSignFastDesktop
{
    public class SignFastCoreSignatureContainer : IExternalSignatureContainer
    {
        ICollection<Org.BouncyCastle.X509.X509Certificate> chain;
        byte[] digest;
        byte[] sign;


        public SignFastCoreSignatureContainer(ICollection<Org.BouncyCastle.X509.X509Certificate> _chain, byte[] _digest, byte[] _sign)
        {
            chain = _chain;
            digest = _digest;
            sign = _sign;
        }

        public byte[] Sign(Stream data)
        {
            try
            {
                PdfPKCS7 pdfPkcs7 = new PdfPKCS7(null, chain, "SHA256", false);

                ITSAClient timeStamp = null;

                if((int)Settings.Default["PermisoSelloTiempo"] == 1)
                {
                    timeStamp = getTimeStampResponse();
                }

                pdfPkcs7.SetExternalDigest(sign, null, "RSA");

                return pdfPkcs7.GetEncodedPKCS7(digest, timeStamp, null, null, CryptoStandard.CMS);

            }
            catch (Exception excepcion)
            {
                throw excepcion;
            }
        }

        public void ModifySigningDictionary(PdfDictionary signDic)
        {
            throw new NotImplementedException();
        }

        public ITSAClient getTimeStampResponse()
        {
            ITSAClient signTSAClient = null;

            try
            {
                ITSAClient tscFinal = new TSAClientBouncyCastle((string)Settings.Default["Url"], (string)Settings.Default["Usuario"], (string)Settings.Default["Contrasenia"]);
                signTSAClient = tscFinal;
            }
            catch(Exception e)
            {
                throw e;
            }
            return signTSAClient;
        }

    }
}
