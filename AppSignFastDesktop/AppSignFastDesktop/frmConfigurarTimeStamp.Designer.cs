﻿namespace AppSignFastDesktop
{
    partial class frmConfigurarTimeStamp
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnAceptar = new System.Windows.Forms.Button();
            this.gbConfiguracionFirmaHolografica = new System.Windows.Forms.GroupBox();
            this.cbSelloTiempo = new System.Windows.Forms.CheckBox();
            this.txtUrl = new System.Windows.Forms.TextBox();
            this.txtContrasenia = new System.Windows.Forms.TextBox();
            this.txtUsuario = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.gbConfiguracionFirmaHolografica.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnAceptar
            // 
            this.btnAceptar.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnAceptar.ForeColor = System.Drawing.Color.White;
            this.btnAceptar.Location = new System.Drawing.Point(386, 157);
            this.btnAceptar.Name = "btnAceptar";
            this.btnAceptar.Size = new System.Drawing.Size(89, 29);
            this.btnAceptar.TabIndex = 11;
            this.btnAceptar.Text = "Guardar";
            this.btnAceptar.UseVisualStyleBackColor = true;
            this.btnAceptar.Click += new System.EventHandler(this.btnAceptar_Click);
            // 
            // gbConfiguracionFirmaHolografica
            // 
            this.gbConfiguracionFirmaHolografica.Controls.Add(this.cbSelloTiempo);
            this.gbConfiguracionFirmaHolografica.Controls.Add(this.txtUrl);
            this.gbConfiguracionFirmaHolografica.Controls.Add(this.txtContrasenia);
            this.gbConfiguracionFirmaHolografica.Controls.Add(this.txtUsuario);
            this.gbConfiguracionFirmaHolografica.Controls.Add(this.label4);
            this.gbConfiguracionFirmaHolografica.Controls.Add(this.label5);
            this.gbConfiguracionFirmaHolografica.Controls.Add(this.label6);
            this.gbConfiguracionFirmaHolografica.Location = new System.Drawing.Point(12, 12);
            this.gbConfiguracionFirmaHolografica.Name = "gbConfiguracionFirmaHolografica";
            this.gbConfiguracionFirmaHolografica.Size = new System.Drawing.Size(463, 139);
            this.gbConfiguracionFirmaHolografica.TabIndex = 12;
            this.gbConfiguracionFirmaHolografica.TabStop = false;
            // 
            // cbSelloTiempo
            // 
            this.cbSelloTiempo.AutoSize = true;
            this.cbSelloTiempo.Location = new System.Drawing.Point(6, 0);
            this.cbSelloTiempo.Name = "cbSelloTiempo";
            this.cbSelloTiempo.Size = new System.Drawing.Size(102, 17);
            this.cbSelloTiempo.TabIndex = 22;
            this.cbSelloTiempo.Text = "Sello de Tiempo";
            this.cbSelloTiempo.UseVisualStyleBackColor = true;
            this.cbSelloTiempo.CheckedChanged += new System.EventHandler(this.cbSelloTiempo_CheckedChanged);
            // 
            // txtUrl
            // 
            this.txtUrl.Location = new System.Drawing.Point(91, 28);
            this.txtUrl.MaxLength = 120;
            this.txtUrl.Name = "txtUrl";
            this.txtUrl.Size = new System.Drawing.Size(355, 20);
            this.txtUrl.TabIndex = 6;
            // 
            // txtContrasenia
            // 
            this.txtContrasenia.Location = new System.Drawing.Point(91, 88);
            this.txtContrasenia.MaxLength = 30;
            this.txtContrasenia.Name = "txtContrasenia";
            this.txtContrasenia.Size = new System.Drawing.Size(142, 20);
            this.txtContrasenia.TabIndex = 5;
            this.txtContrasenia.TextChanged += new System.EventHandler(this.txtContrasenia_TextChanged);
            // 
            // txtUsuario
            // 
            this.txtUsuario.Location = new System.Drawing.Point(91, 58);
            this.txtUsuario.MaxLength = 30;
            this.txtUsuario.Name = "txtUsuario";
            this.txtUsuario.Size = new System.Drawing.Size(142, 20);
            this.txtUsuario.TabIndex = 4;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(60, 31);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(29, 13);
            this.label4.TabIndex = 1;
            this.label4.Text = "URL";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(28, 92);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(61, 13);
            this.label5.TabIndex = 2;
            this.label5.Text = "Contraseña";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(46, 61);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(43, 13);
            this.label6.TabIndex = 3;
            this.label6.Text = "Usuario";
            // 
            // frmConfigurarTimeStamp
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.ClientSize = new System.Drawing.Size(487, 193);
            this.Controls.Add(this.gbConfiguracionFirmaHolografica);
            this.Controls.Add(this.btnAceptar);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "frmConfigurarTimeStamp";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Configuración de sello de tiempo";
            this.Load += new System.EventHandler(this.frmConfigurarTimeStamp_Load);
            this.gbConfiguracionFirmaHolografica.ResumeLayout(false);
            this.gbConfiguracionFirmaHolografica.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button btnAceptar;
        private System.Windows.Forms.GroupBox gbConfiguracionFirmaHolografica;
        private System.Windows.Forms.TextBox txtUrl;
        private System.Windows.Forms.TextBox txtContrasenia;
        private System.Windows.Forms.TextBox txtUsuario;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.CheckBox cbSelloTiempo;
    }
}