﻿using AppSignFastDesktop.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AppSignFastDesktop
{
    public partial class frmConfiguracionCoordenadas : Form
    {
        public frmConfiguracionCoordenadas()
        {
            InitializeComponent();
        }

        //private void btnAceptar_Click(object sender, EventArgs e)
        //{
        //    int permisoSufijo = 0;

        //    if (cbSufijo.Checked)
        //    {
        //        permisoSufijo = 1;

        //        if(txtSufijo.Text == "")
        //        {
        //            MessageBox.Show("Debe agregar un sufijo", "SignFastDesktop", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        //            return;
        //        }

        //    }

        //    Settings.Default["TextoSufijo"] = txtSufijo.Text;
        //    Settings.Default["PermisoSufijo"] = permisoSufijo;
        //    Settings.Default.Save();

        //    MessageBox.Show("Los datos fueron guardados exitosamente.", "SignFastDesktop", MessageBoxButtons.OK, MessageBoxIcon.Information);
        //    this.Close();
        //}

        //private void cbSufijo_CheckedChanged(object sender, EventArgs e)
        //{
        //    if (cbSufijo.Checked)
        //    {
        //        txtSufijo.Enabled = true;
        //    }
        //    else
        //    {
        //        txtSufijo.Enabled = false;
        //    }
        //}

        //private void frmConfiguracionSufijo_Load(object sender, EventArgs e)
        //{
        //    if((int)Settings.Default["PermisoSufijo"] == 1)
        //    {
        //        cbSufijo.Checked = true;
        //        txtSufijo.Enabled = true;
        //    }
        //    else
        //    {
        //        cbSufijo.Checked = false;
        //        txtSufijo.Enabled = false;
        //    }

        //    txtSufijo.Text = (string)Settings.Default["TextoSufijo"];
        //}

        //private void FrmConfiguracionSufijo_Load_1(object sender, EventArgs e)
        //{

        //}

        //private void Button1_Click(object sender, EventArgs e)
        //{

        //}

        private void FrmConfiguracionSufijo_Load_2(object sender, EventArgs e)
        {

        }

        private void BtnGuardar_Click(object sender, EventArgs e)
        {

            if (txtNombre.Text == "")
            {
                MessageBox.Show("El campo nombre no debe ser vacío", "SignFastDesktop", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            if (txtX.Text == "")
            {
                MessageBox.Show("El campo X no debe ser vacío", "SignFastDesktop", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            if (txtY.Text == "")
            {
                MessageBox.Show("El campo Y no debe ser vacío", "SignFastDesktop", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            string coordenadasGuardadas = (string)Settings.Default["Coordenadas"];

            if (!coordenadasGuardadas.Equals(""))
            {
                string coordenadas = txtNombre.Text + "," + txtX.Text + "," + txtY.Text + ";";
                Settings.Default["Coordenadas"] = coordenadasGuardadas+coordenadas;
                Settings.Default.Save();
            }
            else
            {
                string coordenadas = txtNombre.Text + "," + txtX.Text + "," + txtY.Text + ";";
                Settings.Default["Coordenadas"] = coordenadas;
                Settings.Default.Save();
            }
       
            MessageBox.Show("Los datos fueron guardados exitosamente.", "SignFastDesktop", MessageBoxButtons.OK, MessageBoxIcon.Information);
            this.Close();
        }

        private void BtnResetear_Click(object sender, EventArgs e)
        {
            string coordenadas = (string)Settings.Default["Coordenadas"];
            string coordenadasVacias = coordenadas.Replace(coordenadas, "");

            Settings.Default["Coordenadas"] = coordenadasVacias;
            Settings.Default.Save();

            MessageBox.Show("Los datos fueron guardados exitosamente.", "SignFastDesktop", MessageBoxButtons.OK, MessageBoxIcon.Information);
            this.Close();
        }
    }
}
