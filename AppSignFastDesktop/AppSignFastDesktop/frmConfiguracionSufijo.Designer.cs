﻿namespace AppSignFastDesktop
{
    partial class frmConfiguracionSufijo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbConfiguracionFirmaHolografica = new System.Windows.Forms.GroupBox();
            this.cbSufijo = new System.Windows.Forms.CheckBox();
            this.txtSufijo = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.btnAceptar = new System.Windows.Forms.Button();
            this.gbConfiguracionFirmaHolografica.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbConfiguracionFirmaHolografica
            // 
            this.gbConfiguracionFirmaHolografica.Controls.Add(this.cbSufijo);
            this.gbConfiguracionFirmaHolografica.Controls.Add(this.txtSufijo);
            this.gbConfiguracionFirmaHolografica.Controls.Add(this.label4);
            this.gbConfiguracionFirmaHolografica.Location = new System.Drawing.Point(12, 12);
            this.gbConfiguracionFirmaHolografica.Name = "gbConfiguracionFirmaHolografica";
            this.gbConfiguracionFirmaHolografica.Size = new System.Drawing.Size(264, 96);
            this.gbConfiguracionFirmaHolografica.TabIndex = 13;
            this.gbConfiguracionFirmaHolografica.TabStop = false;
            // 
            // cbSufijo
            // 
            this.cbSufijo.AutoSize = true;
            this.cbSufijo.Location = new System.Drawing.Point(6, 0);
            this.cbSufijo.Name = "cbSufijo";
            this.cbSufijo.Size = new System.Drawing.Size(52, 17);
            this.cbSufijo.TabIndex = 22;
            this.cbSufijo.Text = "Sufijo";
            this.cbSufijo.UseVisualStyleBackColor = true;
            this.cbSufijo.CheckedChanged += new System.EventHandler(this.cbSufijo_CheckedChanged);
            // 
            // txtSufijo
            // 
            this.txtSufijo.Location = new System.Drawing.Point(72, 28);
            this.txtSufijo.Name = "txtSufijo";
            this.txtSufijo.Size = new System.Drawing.Size(107, 20);
            this.txtSufijo.TabIndex = 6;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(18, 31);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(33, 13);
            this.label4.TabIndex = 1;
            this.label4.Text = "Sufijo";
            // 
            // btnAceptar
            // 
            this.btnAceptar.Location = new System.Drawing.Point(201, 119);
            this.btnAceptar.Name = "btnAceptar";
            this.btnAceptar.Size = new System.Drawing.Size(75, 23);
            this.btnAceptar.TabIndex = 14;
            this.btnAceptar.Text = "Aceptar";
            this.btnAceptar.UseVisualStyleBackColor = true;
            this.btnAceptar.Click += new System.EventHandler(this.btnAceptar_Click);
            // 
            // frmConfiguracionSufijo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(299, 154);
            this.Controls.Add(this.btnAceptar);
            this.Controls.Add(this.gbConfiguracionFirmaHolografica);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "frmConfiguracionSufijo";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Configuracion Sufijo";
            this.Load += new System.EventHandler(this.frmConfiguracionSufijo_Load);
            this.gbConfiguracionFirmaHolografica.ResumeLayout(false);
            this.gbConfiguracionFirmaHolografica.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbConfiguracionFirmaHolografica;
        private System.Windows.Forms.CheckBox cbSufijo;
        private System.Windows.Forms.TextBox txtSufijo;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnAceptar;
    }
}