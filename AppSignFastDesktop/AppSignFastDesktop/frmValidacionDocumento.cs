﻿using iTextSharp.text.pdf;
using iTextSharp.text.pdf.security;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AppSignFastDesktop
{
    public partial class frmValidacionDocumento : Form
    {
        public frmValidacionDocumento()
        {
            InitializeComponent();
        }

        private void btnVerificar_Click(object sender, EventArgs e)
        {
            //StringBuilder cadena = null;


            if (txtDocumento.Text == "")
            {
                MessageBox.Show("Debe seleccionar un documento.", "SignFastDesktop", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            PdfReader reader = new PdfReader(txtDocumento.Text);
            AcroFields fields = reader.AcroFields;

            List<String> names = fields.GetSignatureNames();

            if(names.Count <= 0 )
            {
                txtValidacion.AppendText("\n");
                txtValidacion.AppendText("No se encontraron firmas en el archivo");
            }

            Boolean estadoFirmaDocumento = true;

            foreach (String name in names)
            {

                PdfPKCS7 pkcs7 = fields.VerifySignature(name);

                Org.BouncyCastle.X509.X509Certificate certificado = pkcs7.SigningCertificate;

                String nombreFirmante = CertificateInfo.GetSubjectFields(certificado).GetField("CN");
                String nombreFirmanteAlternativo = pkcs7.SignName != null ? pkcs7.SignName : "";
                String fechaFirma = pkcs7.SignDate.ToString();
                String estadoFirma = "SI";
                if (!pkcs7.Verify())
                {
                    estadoFirma = "NO";
                    estadoFirmaDocumento = false;
                }

                txtValidacion.AppendText("\r\n Nombre del firmante: " + nombreFirmante);
                txtValidacion.AppendText("\r\n Fecha Firma: " + fechaFirma);
                txtValidacion.AppendText("\r\n Valida: " + estadoFirma);
                txtValidacion.AppendText("\r\n");

            }

            if (names.Count > 0)
            {
                if (estadoFirmaDocumento == true)
                {
                    
                    //cadena.Append("<b><font color=red>OK.</font></b>");
                    //txtValidacion.AppendText(cadena.ToString());
                    txtValidacion.AppendText("\r\n Estado Documento: OK");
                }
                else
                {
                    txtValidacion.AppendText("\r\n Estado Documento: INVALIDO");
                }

            }
            btnVerificar.Enabled = false;
        }

        private void frmValidacionDocumento_FormClosing(object sender, FormClosingEventArgs e)
        {
            txtDocumento.Clear();
            txtValidacion.Clear();
        }

        private void frmValidacionDocumento_Load(object sender, EventArgs e)
        {
            //txtDocumento.AppendText("Click aquí para seleccionar un documento ...");
        }

        private void lblAddDocumentos_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            txtValidacion.Clear();

            OpenFileDialog opfRutaDocumento = new OpenFileDialog();
            opfRutaDocumento.DefaultExt = "pdf";
            opfRutaDocumento.Filter = "pdf files (*.pdf)|*.pdf";

            if (opfRutaDocumento.ShowDialog() == DialogResult.OK)
            {
                String ruta = Path.GetFullPath(opfRutaDocumento.FileName);
                txtDocumento.Text = ruta;
            }
            btnVerificar.Enabled = true;
        }
    }
}
