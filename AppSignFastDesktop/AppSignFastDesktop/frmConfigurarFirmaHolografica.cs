﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AppSignFastDesktop.Properties;
using System.IO;

namespace AppSignFastDesktop
{
    public partial class frmConfigurarFirmaHolografica : Form
    {
        string folderImg = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.UserProfile), ".SignFast");
        //FileInfo info = new FileInfo("img");
        string destino;

        public frmConfigurarFirmaHolografica()
        {
            InitializeComponent();
   
            

        }

        private void btnAbrirDialogo_Click(object sender, EventArgs e)
        {
 
            OpenFileDialog opfRutaImagen = new OpenFileDialog();
            opfRutaImagen.Filter = "JPG Image|*.jpg|JPEG Imagen|*.jpeg";
            if (opfRutaImagen.ShowDialog() == DialogResult.OK)
            {
                
                string rutaImagen = opfRutaImagen.FileName;
                string nombreImagen = Path.Combine(folderImg,opfRutaImagen.SafeFileName);

                txtRutaImagen.Text = nombreImagen;
                if (!File.Exists(nombreImagen))
                {
                    File.Copy(rutaImagen, nombreImagen);
                }
              
                pbImagenVistaPrevia.Image = Image.FromFile(txtRutaImagen.Text);
                //Settings.Default["RutaImagen"] = nombreImagen;                   
                //Settings.Default.Save();                                
            }
            
        }

        private void frmConfigurarFirmaHolografica_Load(object sender, EventArgs e)
        {

            
            if ((int)Settings.Default["PermisoFirmaHolografa"] == 1)
            {
                cbHabilitarFirmaHolografica.Checked = true;
                btnAbrirDialogo.Enabled = true;
                txtColegiatura.Enabled = true;
                //txtCargo.Enabled = true;
                cboPagina.Enabled = true;

                //txtDni.Enabled = true;
                txtAncho.Enabled = true;
                txtAlto.Enabled = true;
                //rb15.Enabled = true;
                //rb14.Enabled = true;
                //rb13.Enabled = true;
                //rb12.Enabled = true;
                //rb11.Enabled = true;
                //rb10.Enabled = true;
                //rb9.Enabled = true;
                //rb8.Enabled = true;
                //rb7.Enabled = true;
                //rb6.Enabled = true;
                //rb5.Enabled = true;
                //rb4.Enabled = true;
                //rb3.Enabled = true;
                //rb2.Enabled = true;
                //rb1.Enabled = true;
                rb15.Enabled = false;
                rb14.Enabled = false;
                rb13.Enabled = false;
                rb12.Enabled = false;
                rb11.Enabled = false;
                rb10.Enabled = false;
                rb9.Enabled = false;
                rb8.Enabled = false;
                rb7.Enabled = false;
                rb6.Enabled = false;
                rb5.Enabled = false;
                rb4.Enabled = false;
                rb3.Enabled = false;
                rb2.Enabled = false;
                rb1.Enabled = false;

                txtX.Enabled = true;
                txtY.Enabled = true;

                if ((int)Settings.Default["PermisoPosiciones"] == 1)
                {
                    chkPosicion.Checked = true;
                    rb15.Enabled = true;
                    rb14.Enabled = true;
                    rb13.Enabled = true;
                    rb12.Enabled = true;
                    rb11.Enabled = true;
                    rb10.Enabled = true;
                    rb9.Enabled = true;
                    rb8.Enabled = true;
                    rb7.Enabled = true;
                    rb6.Enabled = true;
                    rb5.Enabled = true;
                    rb4.Enabled = true;
                    rb3.Enabled = true;
                    rb2.Enabled = true;
                    rb1.Enabled = true;
                    txtX.Enabled = false;
                    txtY.Enabled = false;
                }
            }
            else
            {
                cbHabilitarFirmaHolografica.Checked = false;
                btnAbrirDialogo.Enabled = false;
                txtColegiatura.Enabled = false;
                //txtCargo.Enabled = false;
                cboPagina.Enabled = false;
                //txtDni.Enabled = false;
                txtAncho.Enabled = false;
                txtAlto.Enabled = false;
                rb15.Enabled = false;
                rb14.Enabled = false;
                rb13.Enabled = false;
                rb12.Enabled = false;
                rb11.Enabled = false;
                rb10.Enabled = false;
                rb9.Enabled = false;
                rb8.Enabled = false;
                rb7.Enabled = false;
                rb6.Enabled = false;
                rb5.Enabled = false;
                rb4.Enabled = false;
                rb3.Enabled = false;
                rb2.Enabled = false;
                rb1.Enabled = false;

                txtX.Enabled = false;
                txtY.Enabled = false;
            }

            if (!File.Exists((string)Settings.Default["RutaImagen"]))
            {
                txtRutaImagen.Text = "";
                Settings.Default["RutaImagen"] = "";
            }
            else
            {
                txtRutaImagen.Text = (string)Settings.Default["RutaImagen"];
                pbImagenVistaPrevia.Image = Image.FromFile((string)Settings.Default["RutaImagen"]);
            }
            
            txtColegiatura.Text = (string)Settings.Default["Motivo"];
            //txtCargo.Text = (string)Settings.Default["Cargo"];
            //txtDni.Text = (string)Settings.Default["Dni"];
            int ancho = (int)Settings.Default["Ancho"];
            int alto = (int)Settings.Default["Alto"];
            txtAncho.Text = ancho.ToString();
            txtAlto.Text = alto.ToString();
            cboPagina.Text = (string)Settings.Default["Pagina"];

            if ((int)Settings.Default["X"] != 0 && (int)Settings.Default["Y"] != 0)
            {
                int parametroX = (int)Settings.Default["X"];
                txtX.Text = parametroX.ToString();

                int parametroY = (int)Settings.Default["Y"];
                txtY.Text = parametroY.ToString();
            }
            else
            {
                txtX.Text = "0";
                txtY.Text = "0";               
            }

            switch ((int)Settings.Default["Posicion"])
            {
                case 1:
                    rb15.Checked = false;
                    rb14.Checked = false;
                    rb13.Checked = false;
                    rb12.Checked = false;
                    rb11.Checked = false;
                    rb10.Checked = false;
                    rb9.Checked = false;
                    rb8.Checked = false;
                    rb7.Checked = false;
                    rb6.Checked = false;
                    rb5.Checked = false;
                    rb4.Checked = false;
                    rb3.Checked = false;
                    rb2.Checked = false;
                    rb1.Checked = true;
                    break;
                case 2:
                    rb15.Checked = false;
                    rb14.Checked = false;
                    rb13.Checked = false;
                    rb12.Checked = false;
                    rb11.Checked = false;
                    rb10.Checked = false;
                    rb9.Checked = false;
                    rb8.Checked = false;
                    rb7.Checked = false;
                    rb6.Checked = false;
                    rb5.Checked = false;
                    rb4.Checked = false;
                    rb3.Checked = false;
                    rb2.Checked = true;
                    rb1.Checked = false;
                    break;
                case 3:
                    rb15.Checked = false;
                    rb14.Checked = false;
                    rb13.Checked = false;
                    rb12.Checked = false;
                    rb11.Checked = false;
                    rb10.Checked = false;
                    rb9.Checked = false;
                    rb8.Checked = false;
                    rb7.Checked = false;
                    rb6.Checked = false;
                    rb5.Checked = false;
                    rb4.Checked = false;
                    rb3.Checked = true;
                    rb2.Checked = false;
                    rb1.Checked = false;
                    break;
                case 4:
                    rb15.Checked = false;
                    rb14.Checked = false;
                    rb13.Checked = false;
                    rb12.Checked = false;
                    rb11.Checked = false;
                    rb10.Checked = false;
                    rb9.Checked = false;
                    rb8.Checked = false;
                    rb7.Checked = false;
                    rb6.Checked = false;
                    rb5.Checked = false;
                    rb4.Checked = true;
                    rb3.Checked = false;
                    rb2.Checked = false;
                    rb1.Checked = false;
                    break;
                case 5:
                    rb15.Checked = false;
                    rb14.Checked = false;
                    rb13.Checked = false;
                    rb12.Checked = false;
                    rb11.Checked = false;
                    rb10.Checked = false;
                    rb9.Checked = false;
                    rb8.Checked = false;
                    rb7.Checked = false;
                    rb6.Checked = false;
                    rb5.Checked = true;
                    rb4.Checked = false;
                    rb3.Checked = false;
                    rb2.Checked = false;
                    rb1.Checked = false;
                    break;
                case 6:
                    rb15.Checked = false;
                    rb14.Checked = false;
                    rb13.Checked = false;
                    rb12.Checked = false;
                    rb11.Checked = false;
                    rb10.Checked = false;
                    rb9.Checked = false;
                    rb8.Checked = false;
                    rb7.Checked = false;
                    rb6.Checked = true;
                    rb5.Checked = false;
                    rb4.Checked = false;
                    rb3.Checked = false;
                    rb2.Checked = false;
                    rb1.Checked = false;
                    break;
                case 7:
                    rb15.Checked = false;
                    rb14.Checked = false;
                    rb13.Checked = false;
                    rb12.Checked = false;
                    rb11.Checked = false;
                    rb10.Checked = false;
                    rb9.Checked = false;
                    rb8.Checked = false;
                    rb7.Checked = true;
                    rb6.Checked = false;
                    rb5.Checked = false;
                    rb4.Checked = false;
                    rb3.Checked = false;
                    rb2.Checked = false;
                    rb1.Checked = false;
                    break;
                case 8:
                    rb15.Checked = false;
                    rb14.Checked = false;
                    rb13.Checked = false;
                    rb12.Checked = false;
                    rb11.Checked = false;
                    rb10.Checked = false;
                    rb9.Checked = false;
                    rb8.Checked = true;
                    rb7.Checked = false;
                    rb6.Checked = false;
                    rb5.Checked = false;
                    rb4.Checked = false;
                    rb3.Checked = false;
                    rb2.Checked = false;
                    rb1.Checked = false;
                    break;
                case 9:
                    rb15.Checked = false;
                    rb14.Checked = false;
                    rb13.Checked = false;
                    rb12.Checked = false;
                    rb11.Checked = false;
                    rb10.Checked = false;
                    rb9.Checked = true;
                    rb8.Checked = false;
                    rb7.Checked = false;
                    rb6.Checked = false;
                    rb5.Checked = false;
                    rb4.Checked = false;
                    rb3.Checked = false;
                    rb2.Checked = false;
                    rb1.Checked = false;
                    break;
                case 10:
                    rb15.Checked = false;
                    rb14.Checked = false;
                    rb13.Checked = false;
                    rb12.Checked = false;
                    rb11.Checked = false;
                    rb10.Checked = true;
                    rb9.Checked = false;
                    rb8.Checked = false;
                    rb7.Checked = false;
                    rb6.Checked = false;
                    rb5.Checked = false;
                    rb4.Checked = false;
                    rb3.Checked = false;
                    rb2.Checked = false;
                    rb1.Checked = false;
                    break;
                case 11:
                    rb15.Checked = false;
                    rb14.Checked = false;
                    rb13.Checked = false;
                    rb12.Checked = false;
                    rb11.Checked = true;
                    rb10.Checked = false;
                    rb9.Checked = false;
                    rb8.Checked = false;
                    rb7.Checked = false;
                    rb6.Checked = false;
                    rb5.Checked = false;
                    rb4.Checked = false;
                    rb3.Checked = false;
                    rb2.Checked = false;
                    rb1.Checked = false;
                    break;
                case 12:
                    rb15.Checked = false;
                    rb14.Checked = false;
                    rb13.Checked = false;
                    rb12.Checked = true;
                    rb11.Checked = false;
                    rb10.Checked = false;
                    rb9.Checked = false;
                    rb8.Checked = false;
                    rb7.Checked = false;
                    rb6.Checked = false;
                    rb5.Checked = false;
                    rb4.Checked = false;
                    rb3.Checked = false;
                    rb2.Checked = false;
                    rb1.Checked = false;
                    break;
                case 13:
                    rb15.Checked = false;
                    rb14.Checked = false;
                    rb13.Checked = true;
                    rb12.Checked = false;
                    rb11.Checked = false;
                    rb10.Checked = false;
                    rb9.Checked = false;
                    rb8.Checked = false;
                    rb7.Checked = false;
                    rb6.Checked = false;
                    rb5.Checked = false;
                    rb4.Checked = false;
                    rb3.Checked = false;
                    rb2.Checked = false;
                    rb1.Checked = false;
                    break;
                case 14:
                    rb15.Checked = false;
                    rb14.Checked = true;
                    rb13.Checked = false;
                    rb12.Checked = false;
                    rb11.Checked = false;
                    rb10.Checked = false;
                    rb9.Checked = false;
                    rb8.Checked = false;
                    rb7.Checked = false;
                    rb6.Checked = false;
                    rb5.Checked = false;
                    rb4.Checked = false;
                    rb3.Checked = false;
                    rb2.Checked = false;
                    rb1.Checked = false;
                    break;
                case 15:
                    rb15.Checked = true;
                    rb14.Checked = false;
                    rb13.Checked = false;
                    rb12.Checked = false;
                    rb11.Checked = false;
                    rb10.Checked = false;
                    rb9.Checked = false;
                    rb8.Checked = false;
                    rb7.Checked = false;
                    rb6.Checked = false;
                    rb5.Checked = false;
                    rb4.Checked = false;
                    rb3.Checked = false;
                    rb2.Checked = false;
                    rb1.Checked = false;
                    break;
            }


            //cboCoordenadas.Items.Clear();
            //string value= (string)Settings.Default["CoordenadaSeleccionada"];
            //cboCoordenadas.Items.Add("NINGUNO");

            //string coordenadas = (string)Settings.Default["Coordenadas"];

            //string[] coordenadasSplit = coordenadas.Split(';');

            //for (int i=0; i < coordenadasSplit.Length; i++)
            //{
            //    if (!coordenadasSplit[i].Equals(""))
            //    {
            //        string[] parametrosSplit = coordenadasSplit[i].Split(',');
            //        cboCoordenadas.Items.Add(parametrosSplit[0]);
            //    }
               
            //}
            //cboCoordenadas.SelectedItem = value;


        }

        private void btnAceptar_Click(object sender, EventArgs e)
        {
            int permisoFimaHolografica = 0;
            int permisoPosiciones = 0;
            int posicion = 0;
            if (rb1.Checked)
            {
                posicion = 1;
            }
            else if (rb2.Checked)
            {
                posicion = 2;
            }
            else if (rb3.Checked)
            {
                posicion = 3;
            }
            else if (rb4.Checked)
            {
                posicion = 4;
            }
            else if (rb5.Checked)
            {
                posicion = 5;
            }
            else if (rb6.Checked)
            {
                posicion = 6;
            }
            else if (rb7.Checked)
            {
                posicion = 7;
            }
            else if (rb8.Checked)
            {
                posicion = 8;
            }
            else if (rb9.Checked)
            {
                posicion = 9;
            }
            else if (rb10.Checked)
            {
                posicion = 10;
            }
            else if (rb11.Checked)
            {
                posicion = 11;
            }
            else if (rb12.Checked)
            {
                posicion = 12;
            }
            else if (rb13.Checked)
            {
                posicion = 13;
            }
            else if (rb14.Checked)
            {
                posicion = 14;
            }
            else if (rb15.Checked)
            {
                posicion = 15;
            }

            if (cbHabilitarFirmaHolografica.Checked)
            {
                permisoFimaHolografica = 1;
            }

            if (chkPosicion.Checked)
            {
                permisoPosiciones = 1;
            }

            if (cbHabilitarFirmaHolografica.Checked)
            {
                //if (txtColegiatura.Text == "")
                //{
                //    MessageBox.Show("Debe agregar un motivo", "SignFastDesktop", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                //    return;
                //}

                if (!chkPosicion.Checked)
                {
                    if (txtX.Text == "0")
                    {
                        MessageBox.Show("El campo posicionX no puede estar en 0", "SignFastClient", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        return;
                    }

                    if (txtY.Text == "0")
                    {
                        MessageBox.Show("El campo posicionY no puede estar en 0", "SignFastClient", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        return;
                    }
                }              

                //if (txtRutaImagen.Text == "")
                //{
                //    MessageBox.Show("Debe agregar una ruta imagen", "SignFastDesktop", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                //    return;
                //}

                if (txtAlto.Text == "0")
                {
                    MessageBox.Show("El campo Alto no puede ser 0", "SignFastDesktop", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

                if (txtAncho.Text == "0")
                {
                    MessageBox.Show("El campo Ancho no puede ser 0", "SignFastDesktop", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

                if (chkPosicion.Checked && posicion == 0)
                {
                    posicion = 0;
                    limpiarRadioButtons();
                    MessageBox.Show("Debe seleccionar alguna posición.", "SignFastClient", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return;
                }
            }

            int posicionX = Convert.ToInt32(txtX.Text);
            int posicionY = Convert.ToInt32(txtY.Text);

            Settings.Default["Posicion"] = posicion;           
            Settings.Default["RutaImagen"] = txtRutaImagen.Text;          
            Settings.Default["Motivo"] = txtColegiatura.Text;
            Settings.Default["Pagina"] = cboPagina.Text;
            Settings.Default["PermisoFirmaHolografa"] = permisoFimaHolografica;
            Settings.Default["PermisoPosiciones"] = permisoPosiciones;
            //Settings.Default["Cargo"] = txtCargo.Text;
            Settings.Default["X"] = posicionX;
            Settings.Default["Y"] = posicionY;
            //Settings.Default["CoordenadaSeleccionada"] = cboCoordenadas.Text;
            //Settings.Default["Dni"] = txtDni.Text;
            Settings.Default["Ancho"] = Convert.ToInt32(txtAncho.Text);
            Settings.Default["Alto"] = Convert.ToInt32(txtAlto.Text);

            Settings.Default.Save();

            //if(txtRutaImagen.Text != "")
            //{
            //    Bitmap b = new Bitmap((string)Settings.Default["RutaImagen"]);
            //    b.

            //}



            MessageBox.Show("Los datos fueron guardados exitosamente.", "SignFastDesktop", MessageBoxButtons.OK, MessageBoxIcon.Information);

            this.Close();

        }

        private void cbHabilitarFirmaHolografica_CheckedChanged(object sender, EventArgs e)
        {
            if (cbHabilitarFirmaHolografica.Checked)
            {
                btnAbrirDialogo.Enabled = true;
                txtColegiatura.Enabled = true;
                //txtCargo.Enabled = true;
                cboPagina.Enabled = true;
                rb15.Enabled = true;
                rb14.Enabled = true;
                rb13.Enabled = true;
                rb12.Enabled = true;
                rb11.Enabled = true;
                rb10.Enabled = true;
                rb9.Enabled = true;
                rb8.Enabled = true;
                rb7.Enabled = true;
                rb6.Enabled = true;
                rb5.Enabled = true;
                rb4.Enabled = true;
                rb3.Enabled = true;
                rb2.Enabled = true;
                rb1.Enabled = true;

                txtX.Enabled = false;
                txtY.Enabled = false;
                chkPosicion.Enabled = true;
                chkPosicion.Checked = true;
                //txtDni.Enabled = true;
                txtAlto.Enabled = true;
                txtAncho.Enabled = true;
            }
            else
            {
                btnAbrirDialogo.Enabled = false;
                txtColegiatura.Enabled = false;
                //txtCargo.Enabled = false;
                cboPagina.Enabled = false;
                rb15.Enabled = false;
                rb14.Enabled = false;
                rb13.Enabled = false;
                rb12.Enabled = false;
                rb11.Enabled = false;
                rb10.Enabled = false;
                rb9.Enabled = false;
                rb8.Enabled = false;
                rb7.Enabled = false;
                rb6.Enabled = false;
                rb5.Enabled = false;
                rb4.Enabled = false;
                rb3.Enabled = false;
                rb2.Enabled = false;
                rb1.Enabled = false;

                txtX.Enabled = false;
                txtY.Enabled = false;
                chkPosicion.Enabled = false;
                //txtDni.Enabled = false;
                txtAlto.Enabled = false;
                txtAncho.Enabled = false;
            }
        }

        private void ChkPosicion_CheckedChanged(object sender, EventArgs e)
        {
            if (chkPosicion.Checked)
            {
                txtX.Enabled = false;
                txtY.Enabled = false;
                //cboCoordenadas.Enabled = false;
                rb15.Enabled = true;
                rb14.Enabled = true;
                rb13.Enabled = true;
                rb12.Enabled = true;
                rb11.Enabled = true;
                rb10.Enabled = true;
                rb9.Enabled = true;
                rb8.Enabled = true;
                rb7.Enabled = true;
                rb6.Enabled = true;
                rb5.Enabled = true;
                rb4.Enabled = true;
                rb3.Enabled = true;
                rb2.Enabled = true;
                rb1.Enabled = true;
            }
            else
            {
                txtX.Enabled = true;
                txtY.Enabled = true;
                //cboCoordenadas.Enabled = true;
                rb15.Enabled = false;
                rb14.Enabled = false;
                rb13.Enabled = false;
                rb12.Enabled = false;
                rb11.Enabled = false;
                rb10.Enabled = false;
                rb9.Enabled = false;
                rb8.Enabled = false;
                rb7.Enabled = false;
                rb6.Enabled = false;
                rb5.Enabled = false;
                rb4.Enabled = false;
                rb3.Enabled = false;
                rb2.Enabled = false;
                rb1.Enabled = false;
            }
        }

        private void limpiarRadioButtons()
        {
            rb1.Checked = false;
            rb2.Checked = false;
            rb3.Checked = false;
            rb4.Checked = false;
            rb5.Checked = false;
            rb6.Checked = false;
            rb7.Checked = false;
            rb8.Checked = false;
            rb9.Checked = false;
            rb10.Checked = false;
            rb11.Checked = false;
            rb12.Checked = false;
            rb13.Checked = false;
            rb14.Checked = false;
            rb15.Checked = false;
        }

        private void CboCoordenadas_SelectedIndexChanged(object sender, EventArgs e)
        {

            //string seleccionadoCombo = cboCoordenadas.Text;

            //string coordenadas = (string)Settings.Default["Coordenadas"];

            //string[] coordenadasSplit = coordenadas.Split(';');

            //if (seleccionadoCombo.Equals("NINGUNO"))
            //{
            //    int parametroX = (int)Settings.Default["X"];
            //    int parametroY = (int)Settings.Default["Y"];

            //    txtX.Enabled = true;
            //    txtY.Enabled = true;
            //    txtY.Enabled = true;
            //    txtY.Enabled = true;
            //    txtY.Enabled = true;
            //    txtY.Enabled = true;

            //    if (parametroX != 0 && parametroY != 0)
            //    {
            //        txtX.Text = parametroX.ToString();
            //        txtY.Text = parametroY.ToString();
            //    }
               
                
            //}
            //else
            //{
            //    for (int i = 0; i < coordenadasSplit.Length; i++)
            //    {
            //        if (!coordenadasSplit[i].Equals(""))
            //        {
            //            string[] parametrosSplit = coordenadasSplit[i].Split(',');
            //            if (parametrosSplit[0].Equals(seleccionadoCombo))
            //            {
            //                txtX.Text = parametrosSplit[1];
            //                txtY.Text = parametrosSplit[2];

            //                txtX.Enabled = false;
            //                txtY.Enabled = false;
            //            }

            //        }

            //    }
            //}        
        }

        private void BtnLimpiar_Click(object sender, EventArgs e)
        {
            txtRutaImagen.Clear();
            pbImagenVistaPrevia.Image = null;
        }
    }
}
