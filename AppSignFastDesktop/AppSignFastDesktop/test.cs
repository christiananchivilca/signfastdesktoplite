﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AppSignFastDesktop
{
    public partial class test : Form
    {
        public test()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            String cadena = "[1]Punto de distribución CRL: Nombre del punto de distribución:Nombre completo:Dirección URL=https://www.oprefa.gob.pe/resources/firmasdigitales/EcRVigIntermedioCRLExpiradoOPREFA.crl";

            var match = Regex.Match(cadena, "URL=(.*)");
            MessageBox.Show("Match: " + match.Groups[1].Value);

        }
    }
}
