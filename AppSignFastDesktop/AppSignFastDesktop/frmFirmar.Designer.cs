﻿namespace AppSignFastDesktop
{
    partial class frmFirmar
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmFirmar));
            this.lstViewCertificados = new System.Windows.Forms.ListView();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.txtDestino = new System.Windows.Forms.TextBox();
            this.btnDestino = new System.Windows.Forms.Button();
            this.ofdAñadir = new System.Windows.Forms.OpenFileDialog();
            this.dgvListaDocumentos = new System.Windows.Forms.DataGridView();
            this.numeroFila = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nombreDocumento = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.idDetalleProcesamiento = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fbdDestino = new System.Windows.Forms.FolderBrowserDialog();
            this.btnFirmarDocumentos = new System.Windows.Forms.Button();
            this.fbdAñadirCarpeta = new System.Windows.Forms.FolderBrowserDialog();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.configuracionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.firmaHolográficaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.timeStampToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.configuracionSufijoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.coordenadasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.validacionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.salirToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.btnQuitar = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lblAddDocumentosIndividualmente = new System.Windows.Forms.LinkLabel();
            this.lblAddDocumentosPorCarpeta = new System.Windows.Forms.LinkLabel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btnQuitarTodos = new System.Windows.Forms.Button();
            this.btnActualizarCertificados = new System.Windows.Forms.LinkLabel();
            ((System.ComponentModel.ISupportInitialize)(this.dgvListaDocumentos)).BeginInit();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // lstViewCertificados
            // 
            this.lstViewCertificados.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lstViewCertificados.FullRowSelect = true;
            this.lstViewCertificados.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
            this.lstViewCertificados.HideSelection = false;
            this.lstViewCertificados.Location = new System.Drawing.Point(15, 55);
            this.lstViewCertificados.Name = "lstViewCertificados";
            this.lstViewCertificados.Size = new System.Drawing.Size(371, 313);
            this.lstViewCertificados.SmallImageList = this.imageList1;
            this.lstViewCertificados.TabIndex = 18;
            this.lstViewCertificados.UseCompatibleStateImageBehavior = false;
            this.lstViewCertificados.View = System.Windows.Forms.View.Details;
            this.lstViewCertificados.Click += new System.EventHandler(this.lstViewCertificados_Click);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "certificado-imagen-03.png");
            // 
            // txtDestino
            // 
            this.txtDestino.Location = new System.Drawing.Point(411, 347);
            this.txtDestino.Name = "txtDestino";
            this.txtDestino.ReadOnly = true;
            this.txtDestino.Size = new System.Drawing.Size(432, 20);
            this.txtDestino.TabIndex = 23;
            // 
            // btnDestino
            // 
            this.btnDestino.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.btnDestino.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnDestino.ForeColor = System.Drawing.Color.White;
            this.btnDestino.Location = new System.Drawing.Point(843, 346);
            this.btnDestino.Name = "btnDestino";
            this.btnDestino.Size = new System.Drawing.Size(32, 22);
            this.btnDestino.TabIndex = 24;
            this.btnDestino.Text = "...";
            this.btnDestino.UseVisualStyleBackColor = false;
            this.btnDestino.Click += new System.EventHandler(this.btnDestino_Click);
            // 
            // dgvListaDocumentos
            // 
            this.dgvListaDocumentos.AllowUserToAddRows = false;
            this.dgvListaDocumentos.AllowUserToDeleteRows = false;
            this.dgvListaDocumentos.BackgroundColor = System.Drawing.SystemColors.ButtonFace;
            this.dgvListaDocumentos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvListaDocumentos.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.numeroFila,
            this.nombreDocumento,
            this.idDetalleProcesamiento});
            this.dgvListaDocumentos.Location = new System.Drawing.Point(411, 55);
            this.dgvListaDocumentos.MultiSelect = false;
            this.dgvListaDocumentos.Name = "dgvListaDocumentos";
            this.dgvListaDocumentos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvListaDocumentos.Size = new System.Drawing.Size(464, 268);
            this.dgvListaDocumentos.TabIndex = 27;
            this.dgvListaDocumentos.DoubleClick += new System.EventHandler(this.dgvListaDocumentos_DoubleClick);
            // 
            // numeroFila
            // 
            this.numeroFila.HeaderText = "Nº";
            this.numeroFila.Name = "numeroFila";
            this.numeroFila.ReadOnly = true;
            this.numeroFila.Width = 30;
            // 
            // nombreDocumento
            // 
            this.nombreDocumento.HeaderText = "Nombre Documento";
            this.nombreDocumento.Name = "nombreDocumento";
            this.nombreDocumento.ReadOnly = true;
            this.nombreDocumento.Width = 300;
            // 
            // idDetalleProcesamiento
            // 
            this.idDetalleProcesamiento.HeaderText = "idDetalleProcesamiento";
            this.idDetalleProcesamiento.Name = "idDetalleProcesamiento";
            this.idDetalleProcesamiento.ReadOnly = true;
            this.idDetalleProcesamiento.Visible = false;
            // 
            // btnFirmarDocumentos
            // 
            this.btnFirmarDocumentos.BackColor = System.Drawing.Color.Goldenrod;
            this.btnFirmarDocumentos.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnFirmarDocumentos.ForeColor = System.Drawing.Color.White;
            this.btnFirmarDocumentos.Location = new System.Drawing.Point(408, 433);
            this.btnFirmarDocumentos.Name = "btnFirmarDocumentos";
            this.btnFirmarDocumentos.Size = new System.Drawing.Size(470, 47);
            this.btnFirmarDocumentos.TabIndex = 28;
            this.btnFirmarDocumentos.Text = "Firmar";
            this.btnFirmarDocumentos.UseVisualStyleBackColor = false;
            this.btnFirmarDocumentos.Click += new System.EventHandler(this.btnFirmarDocumentos_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.SystemColors.Menu;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.configuracionToolStripMenuItem,
            this.validacionToolStripMenuItem,
            this.salirToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(887, 24);
            this.menuStrip1.TabIndex = 30;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // configuracionToolStripMenuItem
            // 
            this.configuracionToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.firmaHolográficaToolStripMenuItem,
            this.timeStampToolStripMenuItem,
            this.configuracionSufijoToolStripMenuItem,
            this.coordenadasToolStripMenuItem});
            this.configuracionToolStripMenuItem.Name = "configuracionToolStripMenuItem";
            this.configuracionToolStripMenuItem.Size = new System.Drawing.Size(95, 20);
            this.configuracionToolStripMenuItem.Text = "Configuración";
            // 
            // firmaHolográficaToolStripMenuItem
            // 
            this.firmaHolográficaToolStripMenuItem.Name = "firmaHolográficaToolStripMenuItem";
            this.firmaHolográficaToolStripMenuItem.Size = new System.Drawing.Size(183, 22);
            this.firmaHolográficaToolStripMenuItem.Text = "Firma holográfica";
            this.firmaHolográficaToolStripMenuItem.Click += new System.EventHandler(this.firmaHolográficaToolStripMenuItem_Click);
            // 
            // timeStampToolStripMenuItem
            // 
            this.timeStampToolStripMenuItem.Name = "timeStampToolStripMenuItem";
            this.timeStampToolStripMenuItem.Size = new System.Drawing.Size(183, 22);
            this.timeStampToolStripMenuItem.Text = "Sello de tiempo";
            this.timeStampToolStripMenuItem.Click += new System.EventHandler(this.timeStampToolStripMenuItem_Click);
            // 
            // configuracionSufijoToolStripMenuItem
            // 
            this.configuracionSufijoToolStripMenuItem.Name = "configuracionSufijoToolStripMenuItem";
            this.configuracionSufijoToolStripMenuItem.Size = new System.Drawing.Size(183, 22);
            this.configuracionSufijoToolStripMenuItem.Text = "Configuracion Sufijo";
            this.configuracionSufijoToolStripMenuItem.Click += new System.EventHandler(this.configuracionSufijoToolStripMenuItem_Click);
            // 
            // coordenadasToolStripMenuItem
            // 
            this.coordenadasToolStripMenuItem.Name = "coordenadasToolStripMenuItem";
            this.coordenadasToolStripMenuItem.Size = new System.Drawing.Size(183, 22);
            this.coordenadasToolStripMenuItem.Text = "Coordenadas";
            this.coordenadasToolStripMenuItem.Visible = false;
            this.coordenadasToolStripMenuItem.Click += new System.EventHandler(this.CoordenadasToolStripMenuItem_Click);
            // 
            // validacionToolStripMenuItem
            // 
            this.validacionToolStripMenuItem.Name = "validacionToolStripMenuItem";
            this.validacionToolStripMenuItem.Size = new System.Drawing.Size(73, 20);
            this.validacionToolStripMenuItem.Text = "Validación";
            this.validacionToolStripMenuItem.Click += new System.EventHandler(this.validacionToolStripMenuItem_Click);
            // 
            // salirToolStripMenuItem
            // 
            this.salirToolStripMenuItem.Name = "salirToolStripMenuItem";
            this.salirToolStripMenuItem.Size = new System.Drawing.Size(41, 20);
            this.salirToolStripMenuItem.Text = "Salir";
            this.salirToolStripMenuItem.Click += new System.EventHandler(this.salirToolStripMenuItem_Click);
            // 
            // btnQuitar
            // 
            this.btnQuitar.BackColor = System.Drawing.Color.Goldenrod;
            this.btnQuitar.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnQuitar.ForeColor = System.Drawing.Color.White;
            this.btnQuitar.Location = new System.Drawing.Point(408, 395);
            this.btnQuitar.Name = "btnQuitar";
            this.btnQuitar.Size = new System.Drawing.Size(236, 32);
            this.btnQuitar.TabIndex = 32;
            this.btnQuitar.Text = "Quitar Individualmente";
            this.btnQuitar.UseVisualStyleBackColor = false;
            this.btnQuitar.Click += new System.EventHandler(this.btnQuitar_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(14, 36);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(145, 15);
            this.label3.TabIndex = 33;
            this.label3.Text = "Seleccione un certificado:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(408, 332);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(156, 15);
            this.label5.TabIndex = 35;
            this.label5.Text = "Seleccione carpeta destino:";
            // 
            // lblAddDocumentosIndividualmente
            // 
            this.lblAddDocumentosIndividualmente.ActiveLinkColor = System.Drawing.Color.Green;
            this.lblAddDocumentosIndividualmente.AutoSize = true;
            this.lblAddDocumentosIndividualmente.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAddDocumentosIndividualmente.LinkColor = System.Drawing.Color.DarkGreen;
            this.lblAddDocumentosIndividualmente.Location = new System.Drawing.Point(408, 36);
            this.lblAddDocumentosIndividualmente.Name = "lblAddDocumentosIndividualmente";
            this.lblAddDocumentosIndividualmente.Size = new System.Drawing.Size(206, 15);
            this.lblAddDocumentosIndividualmente.TabIndex = 36;
            this.lblAddDocumentosIndividualmente.TabStop = true;
            this.lblAddDocumentosIndividualmente.Text = "Añadir documentos individualmente";
            this.lblAddDocumentosIndividualmente.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lblAddDocumentosIndividualmente_LinkClicked);
            // 
            // lblAddDocumentosPorCarpeta
            // 
            this.lblAddDocumentosPorCarpeta.ActiveLinkColor = System.Drawing.Color.Green;
            this.lblAddDocumentosPorCarpeta.AutoSize = true;
            this.lblAddDocumentosPorCarpeta.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAddDocumentosPorCarpeta.LinkColor = System.Drawing.Color.DarkGreen;
            this.lblAddDocumentosPorCarpeta.Location = new System.Drawing.Point(698, 36);
            this.lblAddDocumentosPorCarpeta.Name = "lblAddDocumentosPorCarpeta";
            this.lblAddDocumentosPorCarpeta.Size = new System.Drawing.Size(180, 15);
            this.lblAddDocumentosPorCarpeta.TabIndex = 37;
            this.lblAddDocumentosPorCarpeta.TabStop = true;
            this.lblAddDocumentosPorCarpeta.Text = "Añadir documentos por carpeta";
            this.lblAddDocumentosPorCarpeta.Visible = false;
            this.lblAddDocumentosPorCarpeta.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lblAddDocumentosPorCarpeta_LinkClicked);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.pictureBox1.Image = global::AppSignFastDesktop.Properties.Resources.Logotipo_SignFast;
            this.pictureBox1.Location = new System.Drawing.Point(15, 374);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(371, 132);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 19;
            this.pictureBox1.TabStop = false;
            // 
            // btnQuitarTodos
            // 
            this.btnQuitarTodos.BackColor = System.Drawing.Color.Goldenrod;
            this.btnQuitarTodos.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnQuitarTodos.ForeColor = System.Drawing.Color.White;
            this.btnQuitarTodos.Location = new System.Drawing.Point(650, 395);
            this.btnQuitarTodos.Name = "btnQuitarTodos";
            this.btnQuitarTodos.Size = new System.Drawing.Size(225, 32);
            this.btnQuitarTodos.TabIndex = 32;
            this.btnQuitarTodos.Text = "Quitar Todos";
            this.btnQuitarTodos.UseVisualStyleBackColor = false;
            this.btnQuitarTodos.Click += new System.EventHandler(this.btnQuitarTodos_Click);
            // 
            // btnActualizarCertificados
            // 
            this.btnActualizarCertificados.ActiveLinkColor = System.Drawing.Color.Green;
            this.btnActualizarCertificados.AutoSize = true;
            this.btnActualizarCertificados.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnActualizarCertificados.LinkColor = System.Drawing.Color.DarkGreen;
            this.btnActualizarCertificados.Location = new System.Drawing.Point(279, 37);
            this.btnActualizarCertificados.Name = "btnActualizarCertificados";
            this.btnActualizarCertificados.Size = new System.Drawing.Size(107, 15);
            this.btnActualizarCertificados.TabIndex = 38;
            this.btnActualizarCertificados.TabStop = true;
            this.btnActualizarCertificados.Text = "Cargar certificados";
            this.btnActualizarCertificados.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.btnActualizarCertificados_LinkClicked);
            // 
            // frmFirmar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.ClientSize = new System.Drawing.Size(887, 520);
            this.Controls.Add(this.btnActualizarCertificados);
            this.Controls.Add(this.lblAddDocumentosPorCarpeta);
            this.Controls.Add(this.lblAddDocumentosIndividualmente);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btnQuitarTodos);
            this.Controls.Add(this.btnQuitar);
            this.Controls.Add(this.btnFirmarDocumentos);
            this.Controls.Add(this.dgvListaDocumentos);
            this.Controls.Add(this.btnDestino);
            this.Controls.Add(this.txtDestino);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.lstViewCertificados);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.Name = "frmFirmar";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "SignFastDesktop - Software de firma digital";
            this.Load += new System.EventHandler(this.frmFirmar_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvListaDocumentos)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ListView lstViewCertificados;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TextBox txtDestino;
        private System.Windows.Forms.Button btnDestino;
        private System.Windows.Forms.OpenFileDialog ofdAñadir;
        private System.Windows.Forms.DataGridView dgvListaDocumentos;
        private System.Windows.Forms.DataGridViewTextBoxColumn numeroFila;
        private System.Windows.Forms.DataGridViewTextBoxColumn nombreDocumento;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDetalleProcesamiento;
        private System.Windows.Forms.FolderBrowserDialog fbdDestino;
        private System.Windows.Forms.Button btnFirmarDocumentos;
        private System.Windows.Forms.FolderBrowserDialog fbdAñadirCarpeta;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem configuracionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem firmaHolográficaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem timeStampToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem validacionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem salirToolStripMenuItem;
        private System.Windows.Forms.Button btnQuitar;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.LinkLabel lblAddDocumentosIndividualmente;
        private System.Windows.Forms.LinkLabel lblAddDocumentosPorCarpeta;
        private System.Windows.Forms.Button btnQuitarTodos;
        private System.Windows.Forms.ToolStripMenuItem configuracionSufijoToolStripMenuItem;
        private System.Windows.Forms.LinkLabel btnActualizarCertificados;
        private System.Windows.Forms.ToolStripMenuItem coordenadasToolStripMenuItem;
    }
}

