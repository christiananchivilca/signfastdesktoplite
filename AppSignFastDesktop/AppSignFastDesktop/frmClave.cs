﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AppSignFastDesktop
{
    public partial class frmClave : Form
    {
        public frmClave()
        {
            InitializeComponent();
        }

        private void BtnAceptar_Click(object sender, EventArgs e)
        {
            getText();
            this.Close();
        }

        public string getText()
        {
            return txtClave.Text;
        }

        private void FrmClave_Load(object sender, EventArgs e)
        {
            txtClave.Clear();
        }
    }
}
