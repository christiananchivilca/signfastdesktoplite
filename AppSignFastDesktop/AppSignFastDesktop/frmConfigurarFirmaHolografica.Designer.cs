﻿namespace AppSignFastDesktop
{
    partial class frmConfigurarFirmaHolografica
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbConfiguracionFirmaHolografica = new System.Windows.Forms.GroupBox();
            this.txtAlto = new System.Windows.Forms.TextBox();
            this.txtAncho = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.txtDni = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.cboCoordenadas = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtY = new System.Windows.Forms.TextBox();
            this.txtX = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtCargo = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.cbHabilitarFirmaHolografica = new System.Windows.Forms.CheckBox();
            this.pbImagenVistaPrevia = new System.Windows.Forms.PictureBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.chkPosicion = new System.Windows.Forms.CheckBox();
            this.label6 = new System.Windows.Forms.Label();
            this.rb15 = new System.Windows.Forms.RadioButton();
            this.rb14 = new System.Windows.Forms.RadioButton();
            this.rb13 = new System.Windows.Forms.RadioButton();
            this.rb12 = new System.Windows.Forms.RadioButton();
            this.rb11 = new System.Windows.Forms.RadioButton();
            this.rb10 = new System.Windows.Forms.RadioButton();
            this.rb9 = new System.Windows.Forms.RadioButton();
            this.rb8 = new System.Windows.Forms.RadioButton();
            this.rb7 = new System.Windows.Forms.RadioButton();
            this.rb6 = new System.Windows.Forms.RadioButton();
            this.rb5 = new System.Windows.Forms.RadioButton();
            this.rb4 = new System.Windows.Forms.RadioButton();
            this.rb3 = new System.Windows.Forms.RadioButton();
            this.rb2 = new System.Windows.Forms.RadioButton();
            this.rb1 = new System.Windows.Forms.RadioButton();
            this.cboPagina = new System.Windows.Forms.ComboBox();
            this.txtColegiatura = new System.Windows.Forms.TextBox();
            this.btnAbrirDialogo = new System.Windows.Forms.Button();
            this.txtRutaImagen = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnAceptar = new System.Windows.Forms.Button();
            this.btnLimpiar = new System.Windows.Forms.Button();
            this.gbConfiguracionFirmaHolografica.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbImagenVistaPrevia)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbConfiguracionFirmaHolografica
            // 
            this.gbConfiguracionFirmaHolografica.Controls.Add(this.txtAlto);
            this.gbConfiguracionFirmaHolografica.Controls.Add(this.txtAncho);
            this.gbConfiguracionFirmaHolografica.Controls.Add(this.label10);
            this.gbConfiguracionFirmaHolografica.Controls.Add(this.label11);
            this.gbConfiguracionFirmaHolografica.Controls.Add(this.txtY);
            this.gbConfiguracionFirmaHolografica.Controls.Add(this.txtX);
            this.gbConfiguracionFirmaHolografica.Controls.Add(this.label7);
            this.gbConfiguracionFirmaHolografica.Controls.Add(this.label5);
            this.gbConfiguracionFirmaHolografica.Controls.Add(this.label2);
            this.gbConfiguracionFirmaHolografica.Controls.Add(this.cbHabilitarFirmaHolografica);
            this.gbConfiguracionFirmaHolografica.Controls.Add(this.txtColegiatura);
            this.gbConfiguracionFirmaHolografica.Controls.Add(this.pbImagenVistaPrevia);
            this.gbConfiguracionFirmaHolografica.Controls.Add(this.groupBox1);
            this.gbConfiguracionFirmaHolografica.Controls.Add(this.cboPagina);
            this.gbConfiguracionFirmaHolografica.Controls.Add(this.btnAbrirDialogo);
            this.gbConfiguracionFirmaHolografica.Controls.Add(this.txtRutaImagen);
            this.gbConfiguracionFirmaHolografica.Controls.Add(this.label3);
            this.gbConfiguracionFirmaHolografica.Controls.Add(this.label1);
            this.gbConfiguracionFirmaHolografica.Location = new System.Drawing.Point(11, 12);
            this.gbConfiguracionFirmaHolografica.Name = "gbConfiguracionFirmaHolografica";
            this.gbConfiguracionFirmaHolografica.Size = new System.Drawing.Size(345, 297);
            this.gbConfiguracionFirmaHolografica.TabIndex = 9;
            this.gbConfiguracionFirmaHolografica.TabStop = false;
            // 
            // txtAlto
            // 
            this.txtAlto.Location = new System.Drawing.Point(152, 117);
            this.txtAlto.MaxLength = 20;
            this.txtAlto.Name = "txtAlto";
            this.txtAlto.Size = new System.Drawing.Size(36, 20);
            this.txtAlto.TabIndex = 35;
            // 
            // txtAncho
            // 
            this.txtAncho.Location = new System.Drawing.Point(73, 117);
            this.txtAncho.MaxLength = 20;
            this.txtAncho.Name = "txtAncho";
            this.txtAncho.Size = new System.Drawing.Size(36, 20);
            this.txtAncho.TabIndex = 34;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(118, 120);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(28, 13);
            this.label10.TabIndex = 33;
            this.label10.Text = "Alto:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(26, 120);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(41, 13);
            this.label11.TabIndex = 32;
            this.label11.Text = "Ancho:";
            // 
            // txtDni
            // 
            this.txtDni.Location = new System.Drawing.Point(231, 385);
            this.txtDni.MaxLength = 15;
            this.txtDni.Name = "txtDni";
            this.txtDni.Size = new System.Drawing.Size(85, 20);
            this.txtDni.TabIndex = 31;
            this.txtDni.Visible = false;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(196, 388);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(29, 13);
            this.label9.TabIndex = 30;
            this.label9.Text = "DNI:";
            this.label9.Visible = false;
            // 
            // cboCoordenadas
            // 
            this.cboCoordenadas.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCoordenadas.FormattingEnabled = true;
            this.cboCoordenadas.Items.AddRange(new object[] {
            "NINGUNO"});
            this.cboCoordenadas.Location = new System.Drawing.Point(94, 338);
            this.cboCoordenadas.Name = "cboCoordenadas";
            this.cboCoordenadas.Size = new System.Drawing.Size(108, 21);
            this.cboCoordenadas.TabIndex = 29;
            this.cboCoordenadas.Visible = false;
            this.cboCoordenadas.SelectedIndexChanged += new System.EventHandler(this.CboCoordenadas_SelectedIndexChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(12, 341);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(73, 13);
            this.label8.TabIndex = 28;
            this.label8.Text = "Coordenadas:";
            this.label8.Visible = false;
            // 
            // txtY
            // 
            this.txtY.Location = new System.Drawing.Point(297, 121);
            this.txtY.MaxLength = 20;
            this.txtY.Name = "txtY";
            this.txtY.Size = new System.Drawing.Size(36, 20);
            this.txtY.TabIndex = 27;
            // 
            // txtX
            // 
            this.txtX.Location = new System.Drawing.Point(236, 120);
            this.txtX.MaxLength = 20;
            this.txtX.Name = "txtX";
            this.txtX.Size = new System.Drawing.Size(36, 20);
            this.txtX.TabIndex = 26;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(278, 124);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(17, 13);
            this.label7.TabIndex = 25;
            this.label7.Text = "Y:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(217, 123);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(17, 13);
            this.label5.TabIndex = 24;
            this.label5.Text = "X:";
            // 
            // txtCargo
            // 
            this.txtCargo.Location = new System.Drawing.Point(87, 385);
            this.txtCargo.MaxLength = 75;
            this.txtCargo.Name = "txtCargo";
            this.txtCargo.Size = new System.Drawing.Size(85, 20);
            this.txtCargo.TabIndex = 23;
            this.txtCargo.Visible = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(8, 388);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(38, 13);
            this.label4.TabIndex = 22;
            this.label4.Text = "Cargo:";
            this.label4.Visible = false;
            // 
            // cbHabilitarFirmaHolografica
            // 
            this.cbHabilitarFirmaHolografica.AutoSize = true;
            this.cbHabilitarFirmaHolografica.Location = new System.Drawing.Point(6, 0);
            this.cbHabilitarFirmaHolografica.Name = "cbHabilitarFirmaHolografica";
            this.cbHabilitarFirmaHolografica.Size = new System.Drawing.Size(108, 17);
            this.cbHabilitarFirmaHolografica.TabIndex = 21;
            this.cbHabilitarFirmaHolografica.Text = "Firma Holografica";
            this.cbHabilitarFirmaHolografica.UseVisualStyleBackColor = true;
            this.cbHabilitarFirmaHolografica.CheckedChanged += new System.EventHandler(this.cbHabilitarFirmaHolografica_CheckedChanged);
            // 
            // pbImagenVistaPrevia
            // 
            this.pbImagenVistaPrevia.BackColor = System.Drawing.Color.Gainsboro;
            this.pbImagenVistaPrevia.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pbImagenVistaPrevia.Location = new System.Drawing.Point(27, 158);
            this.pbImagenVistaPrevia.Name = "pbImagenVistaPrevia";
            this.pbImagenVistaPrevia.Size = new System.Drawing.Size(200, 133);
            this.pbImagenVistaPrevia.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbImagenVistaPrevia.TabIndex = 20;
            this.pbImagenVistaPrevia.TabStop = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.chkPosicion);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.rb15);
            this.groupBox1.Controls.Add(this.rb14);
            this.groupBox1.Controls.Add(this.rb13);
            this.groupBox1.Controls.Add(this.rb12);
            this.groupBox1.Controls.Add(this.rb11);
            this.groupBox1.Controls.Add(this.rb10);
            this.groupBox1.Controls.Add(this.rb9);
            this.groupBox1.Controls.Add(this.rb8);
            this.groupBox1.Controls.Add(this.rb7);
            this.groupBox1.Controls.Add(this.rb6);
            this.groupBox1.Controls.Add(this.rb5);
            this.groupBox1.Controls.Add(this.rb4);
            this.groupBox1.Controls.Add(this.rb3);
            this.groupBox1.Controls.Add(this.rb2);
            this.groupBox1.Controls.Add(this.rb1);
            this.groupBox1.Location = new System.Drawing.Point(251, 158);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(81, 133);
            this.groupBox1.TabIndex = 19;
            this.groupBox1.TabStop = false;
            // 
            // chkPosicion
            // 
            this.chkPosicion.AutoSize = true;
            this.chkPosicion.Location = new System.Drawing.Point(7, 0);
            this.chkPosicion.Name = "chkPosicion";
            this.chkPosicion.Size = new System.Drawing.Size(15, 14);
            this.chkPosicion.TabIndex = 30;
            this.chkPosicion.UseVisualStyleBackColor = true;
            this.chkPosicion.CheckedChanged += new System.EventHandler(this.ChkPosicion_CheckedChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(20, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(47, 13);
            this.label6.TabIndex = 15;
            this.label6.Text = "Posición";
            // 
            // rb15
            // 
            this.rb15.AutoSize = true;
            this.rb15.Location = new System.Drawing.Point(60, 99);
            this.rb15.Name = "rb15";
            this.rb15.Size = new System.Drawing.Size(14, 13);
            this.rb15.TabIndex = 14;
            this.rb15.UseVisualStyleBackColor = true;
            // 
            // rb14
            // 
            this.rb14.AutoSize = true;
            this.rb14.Location = new System.Drawing.Point(34, 99);
            this.rb14.Name = "rb14";
            this.rb14.Size = new System.Drawing.Size(14, 13);
            this.rb14.TabIndex = 13;
            this.rb14.UseVisualStyleBackColor = true;
            // 
            // rb13
            // 
            this.rb13.AutoSize = true;
            this.rb13.Location = new System.Drawing.Point(8, 99);
            this.rb13.Name = "rb13";
            this.rb13.Size = new System.Drawing.Size(14, 13);
            this.rb13.TabIndex = 12;
            this.rb13.UseVisualStyleBackColor = true;
            // 
            // rb12
            // 
            this.rb12.AutoSize = true;
            this.rb12.Location = new System.Drawing.Point(60, 77);
            this.rb12.Name = "rb12";
            this.rb12.Size = new System.Drawing.Size(14, 13);
            this.rb12.TabIndex = 11;
            this.rb12.UseVisualStyleBackColor = true;
            // 
            // rb11
            // 
            this.rb11.AutoSize = true;
            this.rb11.Location = new System.Drawing.Point(34, 77);
            this.rb11.Name = "rb11";
            this.rb11.Size = new System.Drawing.Size(14, 13);
            this.rb11.TabIndex = 10;
            this.rb11.UseVisualStyleBackColor = true;
            // 
            // rb10
            // 
            this.rb10.AutoSize = true;
            this.rb10.Location = new System.Drawing.Point(8, 77);
            this.rb10.Name = "rb10";
            this.rb10.Size = new System.Drawing.Size(14, 13);
            this.rb10.TabIndex = 9;
            this.rb10.UseVisualStyleBackColor = true;
            // 
            // rb9
            // 
            this.rb9.AutoSize = true;
            this.rb9.Location = new System.Drawing.Point(60, 59);
            this.rb9.Name = "rb9";
            this.rb9.Size = new System.Drawing.Size(14, 13);
            this.rb9.TabIndex = 8;
            this.rb9.UseVisualStyleBackColor = true;
            // 
            // rb8
            // 
            this.rb8.AutoSize = true;
            this.rb8.Location = new System.Drawing.Point(34, 59);
            this.rb8.Name = "rb8";
            this.rb8.Size = new System.Drawing.Size(14, 13);
            this.rb8.TabIndex = 7;
            this.rb8.UseVisualStyleBackColor = true;
            // 
            // rb7
            // 
            this.rb7.AutoSize = true;
            this.rb7.Location = new System.Drawing.Point(8, 59);
            this.rb7.Name = "rb7";
            this.rb7.Size = new System.Drawing.Size(14, 13);
            this.rb7.TabIndex = 6;
            this.rb7.UseVisualStyleBackColor = true;
            // 
            // rb6
            // 
            this.rb6.AutoSize = true;
            this.rb6.Location = new System.Drawing.Point(60, 40);
            this.rb6.Name = "rb6";
            this.rb6.Size = new System.Drawing.Size(14, 13);
            this.rb6.TabIndex = 5;
            this.rb6.UseVisualStyleBackColor = true;
            // 
            // rb5
            // 
            this.rb5.AutoSize = true;
            this.rb5.Location = new System.Drawing.Point(34, 40);
            this.rb5.Name = "rb5";
            this.rb5.Size = new System.Drawing.Size(14, 13);
            this.rb5.TabIndex = 4;
            this.rb5.UseVisualStyleBackColor = true;
            // 
            // rb4
            // 
            this.rb4.AutoSize = true;
            this.rb4.Location = new System.Drawing.Point(8, 39);
            this.rb4.Name = "rb4";
            this.rb4.Size = new System.Drawing.Size(14, 13);
            this.rb4.TabIndex = 3;
            this.rb4.UseVisualStyleBackColor = true;
            // 
            // rb3
            // 
            this.rb3.AutoSize = true;
            this.rb3.Location = new System.Drawing.Point(60, 19);
            this.rb3.Name = "rb3";
            this.rb3.Size = new System.Drawing.Size(14, 13);
            this.rb3.TabIndex = 2;
            this.rb3.UseVisualStyleBackColor = true;
            // 
            // rb2
            // 
            this.rb2.AutoSize = true;
            this.rb2.Location = new System.Drawing.Point(34, 19);
            this.rb2.Name = "rb2";
            this.rb2.Size = new System.Drawing.Size(14, 13);
            this.rb2.TabIndex = 1;
            this.rb2.UseVisualStyleBackColor = true;
            // 
            // rb1
            // 
            this.rb1.AutoSize = true;
            this.rb1.Checked = true;
            this.rb1.Location = new System.Drawing.Point(8, 19);
            this.rb1.Name = "rb1";
            this.rb1.Size = new System.Drawing.Size(14, 13);
            this.rb1.TabIndex = 0;
            this.rb1.TabStop = true;
            this.rb1.UseVisualStyleBackColor = true;
            // 
            // cboPagina
            // 
            this.cboPagina.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboPagina.FormattingEnabled = true;
            this.cboPagina.Items.AddRange(new object[] {
            "PRIMERA",
            "ULTIMA"});
            this.cboPagina.Location = new System.Drawing.Point(103, 86);
            this.cboPagina.Name = "cboPagina";
            this.cboPagina.Size = new System.Drawing.Size(85, 21);
            this.cboPagina.TabIndex = 15;
            // 
            // txtColegiatura
            // 
            this.txtColegiatura.Location = new System.Drawing.Point(103, 60);
            this.txtColegiatura.MaxLength = 20;
            this.txtColegiatura.Name = "txtColegiatura";
            this.txtColegiatura.Size = new System.Drawing.Size(229, 20);
            this.txtColegiatura.TabIndex = 14;
            // 
            // btnAbrirDialogo
            // 
            this.btnAbrirDialogo.Location = new System.Drawing.Point(297, 25);
            this.btnAbrirDialogo.Name = "btnAbrirDialogo";
            this.btnAbrirDialogo.Size = new System.Drawing.Size(35, 23);
            this.btnAbrirDialogo.TabIndex = 13;
            this.btnAbrirDialogo.Text = "...";
            this.btnAbrirDialogo.UseVisualStyleBackColor = true;
            this.btnAbrirDialogo.Click += new System.EventHandler(this.btnAbrirDialogo_Click);
            // 
            // txtRutaImagen
            // 
            this.txtRutaImagen.BackColor = System.Drawing.SystemColors.Window;
            this.txtRutaImagen.Enabled = false;
            this.txtRutaImagen.Location = new System.Drawing.Point(103, 27);
            this.txtRutaImagen.Name = "txtRutaImagen";
            this.txtRutaImagen.ReadOnly = true;
            this.txtRutaImagen.Size = new System.Drawing.Size(191, 20);
            this.txtRutaImagen.TabIndex = 12;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(24, 89);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(43, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "Página:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(24, 63);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(42, 13);
            this.label2.TabIndex = 10;
            this.label2.Text = "Motivo:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(24, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(70, 13);
            this.label1.TabIndex = 11;
            this.label1.Text = "Ruta imagen:";
            // 
            // btnAceptar
            // 
            this.btnAceptar.BackColor = System.Drawing.Color.Goldenrod;
            this.btnAceptar.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnAceptar.ForeColor = System.Drawing.Color.White;
            this.btnAceptar.Location = new System.Drawing.Point(267, 315);
            this.btnAceptar.Name = "btnAceptar";
            this.btnAceptar.Size = new System.Drawing.Size(89, 29);
            this.btnAceptar.TabIndex = 10;
            this.btnAceptar.Text = "Guardar";
            this.btnAceptar.UseVisualStyleBackColor = false;
            this.btnAceptar.Click += new System.EventHandler(this.btnAceptar_Click);
            // 
            // btnLimpiar
            // 
            this.btnLimpiar.BackColor = System.Drawing.Color.Goldenrod;
            this.btnLimpiar.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnLimpiar.ForeColor = System.Drawing.Color.White;
            this.btnLimpiar.Location = new System.Drawing.Point(17, 315);
            this.btnLimpiar.Name = "btnLimpiar";
            this.btnLimpiar.Size = new System.Drawing.Size(89, 29);
            this.btnLimpiar.TabIndex = 32;
            this.btnLimpiar.Text = "Limpiar Imagen";
            this.btnLimpiar.UseVisualStyleBackColor = false;
            this.btnLimpiar.Click += new System.EventHandler(this.BtnLimpiar_Click);
            // 
            // frmConfigurarFirmaHolografica
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.ClientSize = new System.Drawing.Size(369, 349);
            this.Controls.Add(this.btnLimpiar);
            this.Controls.Add(this.btnAceptar);
            this.Controls.Add(this.gbConfiguracionFirmaHolografica);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtCargo);
            this.Controls.Add(this.cboCoordenadas);
            this.Controls.Add(this.txtDni);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label9);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "frmConfigurarFirmaHolografica";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Configuración de firma holográfica";
            this.Load += new System.EventHandler(this.frmConfigurarFirmaHolografica_Load);
            this.gbConfiguracionFirmaHolografica.ResumeLayout(false);
            this.gbConfiguracionFirmaHolografica.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbImagenVistaPrevia)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox gbConfiguracionFirmaHolografica;
        private System.Windows.Forms.ComboBox cboPagina;
        private System.Windows.Forms.TextBox txtColegiatura;
        private System.Windows.Forms.Button btnAbrirDialogo;
        private System.Windows.Forms.TextBox txtRutaImagen;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pbImagenVistaPrevia;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.RadioButton rb15;
        private System.Windows.Forms.RadioButton rb14;
        private System.Windows.Forms.RadioButton rb13;
        private System.Windows.Forms.RadioButton rb12;
        private System.Windows.Forms.RadioButton rb11;
        private System.Windows.Forms.RadioButton rb10;
        private System.Windows.Forms.RadioButton rb9;
        private System.Windows.Forms.RadioButton rb8;
        private System.Windows.Forms.RadioButton rb7;
        private System.Windows.Forms.RadioButton rb6;
        private System.Windows.Forms.RadioButton rb5;
        private System.Windows.Forms.RadioButton rb4;
        private System.Windows.Forms.RadioButton rb3;
        private System.Windows.Forms.RadioButton rb2;
        private System.Windows.Forms.RadioButton rb1;
        private System.Windows.Forms.Button btnAceptar;
        private System.Windows.Forms.CheckBox cbHabilitarFirmaHolografica;
        private System.Windows.Forms.TextBox txtCargo;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cboCoordenadas;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtY;
        private System.Windows.Forms.TextBox txtX;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.CheckBox chkPosicion;
        private System.Windows.Forms.TextBox txtDni;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtAlto;
        private System.Windows.Forms.TextBox txtAncho;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button btnLimpiar;
    }
}