﻿using AppSignFastDesktop.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AppSignFastDesktop
{
    public partial class frmConfiguracionSufijo : Form
    {
        public frmConfiguracionSufijo()
        {
            InitializeComponent();
        }

        private void btnAceptar_Click(object sender, EventArgs e)
        {
            int permisoSufijo = 0;

            if (cbSufijo.Checked)
            {
                permisoSufijo = 1;

                if(txtSufijo.Text == "")
                {
                    MessageBox.Show("Debe agregar un sufijo", "SignFastDesktop", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

            }

            Settings.Default["TextoSufijo"] = txtSufijo.Text;
            Settings.Default["PermisoSufijo"] = permisoSufijo;
            Settings.Default.Save();

            MessageBox.Show("Los datos fueron guardados exitosamente.", "SignFastDesktop", MessageBoxButtons.OK, MessageBoxIcon.Information);
            this.Close();
        }

        private void cbSufijo_CheckedChanged(object sender, EventArgs e)
        {
            if (cbSufijo.Checked)
            {
                txtSufijo.Enabled = true;
            }
            else
            {
                txtSufijo.Enabled = false;
            }
        }

        private void frmConfiguracionSufijo_Load(object sender, EventArgs e)
        {
            if((int)Settings.Default["PermisoSufijo"] == 1)
            {
                cbSufijo.Checked = true;
                txtSufijo.Enabled = true;
            }
            else
            {
                cbSufijo.Checked = false;
                txtSufijo.Enabled = false;
            }

            txtSufijo.Text = (string)Settings.Default["TextoSufijo"];
        }
    }
}
