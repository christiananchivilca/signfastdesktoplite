﻿namespace AppSignFastDesktop
{
    partial class frmVisorPDF
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmVisorPDF));
            this.axAcroPdfVisor = new AxAcroPDFLib.AxAcroPDF();
            ((System.ComponentModel.ISupportInitialize)(this.axAcroPdfVisor)).BeginInit();
            this.SuspendLayout();
            // 
            // axAcroPdfVisor
            // 
            this.axAcroPdfVisor.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.axAcroPdfVisor.Enabled = true;
            this.axAcroPdfVisor.Location = new System.Drawing.Point(1, 0);
            this.axAcroPdfVisor.Name = "axAcroPdfVisor";
            this.axAcroPdfVisor.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axAcroPdfVisor.OcxState")));
            this.axAcroPdfVisor.Size = new System.Drawing.Size(516, 562);
            this.axAcroPdfVisor.TabIndex = 0;
            // 
            // frmVisorPDF
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(518, 597);
            this.Controls.Add(this.axAcroPdfVisor);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmVisorPDF";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "SignFastDesktop - Visor PDF";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmVisorPDF_FormClosing);
            this.Load += new System.EventHandler(this.frmVisorPDF_Load);
            ((System.ComponentModel.ISupportInitialize)(this.axAcroPdfVisor)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private AxAcroPDFLib.AxAcroPDF axAcroPdfVisor;
    }
}