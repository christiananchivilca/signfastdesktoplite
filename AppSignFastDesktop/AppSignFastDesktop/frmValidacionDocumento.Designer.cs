﻿namespace AppSignFastDesktop
{
    partial class frmValidacionDocumento
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txtDocumento = new System.Windows.Forms.TextBox();
            this.btnVerificar = new System.Windows.Forms.Button();
            this.txtValidacion = new System.Windows.Forms.TextBox();
            this.lblAddDocumentos = new System.Windows.Forms.LinkLabel();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(75, 15);
            this.label1.TabIndex = 12;
            this.label1.Text = "Documento:";
            // 
            // txtDocumento
            // 
            this.txtDocumento.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtDocumento.Location = new System.Drawing.Point(93, 24);
            this.txtDocumento.Name = "txtDocumento";
            this.txtDocumento.ReadOnly = true;
            this.txtDocumento.Size = new System.Drawing.Size(284, 20);
            this.txtDocumento.TabIndex = 13;
            // 
            // btnVerificar
            // 
            this.btnVerificar.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnVerificar.ForeColor = System.Drawing.Color.White;
            this.btnVerificar.Location = new System.Drawing.Point(389, 23);
            this.btnVerificar.Name = "btnVerificar";
            this.btnVerificar.Size = new System.Drawing.Size(120, 22);
            this.btnVerificar.TabIndex = 23;
            this.btnVerificar.Text = "Verificar";
            this.btnVerificar.UseVisualStyleBackColor = true;
            this.btnVerificar.Click += new System.EventHandler(this.btnVerificar_Click);
            // 
            // txtValidacion
            // 
            this.txtValidacion.Location = new System.Drawing.Point(15, 50);
            this.txtValidacion.Multiline = true;
            this.txtValidacion.Name = "txtValidacion";
            this.txtValidacion.ReadOnly = true;
            this.txtValidacion.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtValidacion.Size = new System.Drawing.Size(494, 248);
            this.txtValidacion.TabIndex = 24;
            // 
            // lblAddDocumentos
            // 
            this.lblAddDocumentos.ActiveLinkColor = System.Drawing.Color.Green;
            this.lblAddDocumentos.AutoSize = true;
            this.lblAddDocumentos.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAddDocumentos.LinkColor = System.Drawing.Color.DarkGreen;
            this.lblAddDocumentos.Location = new System.Drawing.Point(90, 6);
            this.lblAddDocumentos.Name = "lblAddDocumentos";
            this.lblAddDocumentos.Size = new System.Drawing.Size(225, 15);
            this.lblAddDocumentos.TabIndex = 37;
            this.lblAddDocumentos.TabStop = true;
            this.lblAddDocumentos.Text = "Añadir documentos para verificar firmar";
            this.lblAddDocumentos.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lblAddDocumentos_LinkClicked);
            // 
            // frmValidacionDocumento
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.ClientSize = new System.Drawing.Size(521, 313);
            this.Controls.Add(this.lblAddDocumentos);
            this.Controls.Add(this.txtValidacion);
            this.Controls.Add(this.btnVerificar);
            this.Controls.Add(this.txtDocumento);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "frmValidacionDocumento";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Verificar Firma";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmValidacionDocumento_FormClosing);
            this.Load += new System.EventHandler(this.frmValidacionDocumento_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtDocumento;
        private System.Windows.Forms.Button btnVerificar;
        private System.Windows.Forms.TextBox txtValidacion;
        private System.Windows.Forms.LinkLabel lblAddDocumentos;
    }
}