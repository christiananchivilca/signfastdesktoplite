﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AppSignFastDesktop
{
    public partial class frmEspera : Form
    {
        public Action Worker { get; set; }

        public frmEspera(Action worker, String mensajeEspera)
        {
            InitializeComponent();

            if (worker == null)
            {
                throw new ArgumentNullException();
            }

            Worker = worker;
            lblMensajeEspera.Text = mensajeEspera;
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            Task.Factory.StartNew(Worker).ContinueWith(t => { this.Close(); }, TaskScheduler.FromCurrentSynchronizationContext());
        }
    }
}
